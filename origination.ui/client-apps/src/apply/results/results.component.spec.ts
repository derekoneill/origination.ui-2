import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { TranslateService } from '@ngx-translate/core';
import { AnalyticsService } from '@ua/shared/analytics';
import {
  Application,
  ApplicationState,
  Decision,
  DecisionStatus,
  Onboarding,
  OnboardingState,
  StoreAddress
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { ResultsComponent } from './results.component';

describe('ResultsComponent', () => {
  let component: ResultsComponent;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockAnalyticsService: SubstituteOf<AnalyticsService>;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;
  let mockTranslateService: SubstituteOf<TranslateService>;

  beforeEach(() => {
    mockApplicationState = Substitute.for<ApplicationState>();
    mockAnalyticsService = Substitute.for<AnalyticsService>();
    mockLayoutService = Substitute.for<LayoutService>();
    mockOnboardingState = Substitute.for<OnboardingState>();
    component = new ResultsComponent(
      mockApplicationState,
      mockAnalyticsService,
      mockLayoutService,
      mockOnboardingState,
      mockTranslateService
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on init', () => {
    const applicationApprovalStatus = new Subject<DecisionStatus>();
    const application = new Subject<Application>();
    const applicationResult = new Subject<Decision>();
    const onboardingSubject = new Subject<Onboarding>();

    beforeEach(() => {
      mockApplicationState.decisionStatus$.returns(
        applicationApprovalStatus.asObservable()
      );
      mockApplicationState.application$.returns(application.asObservable());
      mockApplicationState.decision$.returns(applicationResult.asObservable());
      mockOnboardingState.onboardingInfo$.returns(onboardingSubject.asObservable());
      component.ngOnInit();
    });

    it('should enable timeout feature', () => {
      mockLayoutService.received().enableTimeout();
    });

    it('should get decision status from the application state', () => {
      const expected = chance.pickone([
        DecisionStatus.approved,
        DecisionStatus.denied,
      ]);
      let actual: DecisionStatus;
      component.decisionStatus$.subscribe(status => (actual = status));
      applicationApprovalStatus.next(expected);
      expect(actual).toBe(expected);
    });

    it('should get the application from the application state', () => {
      const expected = { storeId: chance.integer({ min: 1 }) } as Application;
      application.next(expected);
      expect(component.application).toBe(expected);
    });

    it('should get the lease id from the application state', () => {
      const leaseId = chance.integer({ min: 1 });
      const expected = { leaseId } as Decision;

      component.decision$.subscribe(decision => decision);
      applicationResult.next(expected);
      expect(component.leaseId).toBe(leaseId);
    });

    it('should fire an application submission event', () => {
      const leaseId = chance.integer({ min: 1 });
      const decisionStatus = chance.pickone(DecisionStatus);
      const approvalLimit = chance.integer({ min: 1 });
      const expected = {
        leaseId,
        status: decisionStatus,
        approvalLimit,
      } as Decision;

      component.decision$.subscribe(decision => decision);
      applicationResult.next(expected);
      mockAnalyticsService.received().trackApplicationSubmit(
        decisionStatus,
        leaseId,
        approvalLimit
      );
    });

    it('should get the decision from the application state', () => {
      const expected = {
        status: chance.pickone([
          DecisionStatus.approved,
          DecisionStatus.denied,
        ]),
      } as Decision;
      let actual: Decision;
      component.decision$.subscribe(decision => (actual = decision));
      applicationResult.next(expected);
      expect(actual).toBe(expected);
    });

    it('should set the store location information from the onboarding state if not ecom', () => {
      const onboarding = { storeLocation: {
        isEcommerce: false,
        address: {
          state: chance.state()
        } as StoreAddress
      } } as Onboarding;

      onboardingSubject.next(onboarding);
      expect(component.storeInformation).toEqual(onboarding.storeLocation);
    });

    it('should not set the store location information from the onboarding state if ecom', () => {
      const onboarding = { storeLocation: {
        isEcommerce: true,
        address: {
          state: chance.state()
        } as StoreAddress
      } } as Onboarding;

      onboardingSubject.next(onboarding);
      expect(component.storeInformation).toEqual(undefined);
    });

    it('should hide footer', () => {
      mockLayoutService.received().hideFooter();
    });

    it('should hide back button', () => {
      mockLayoutService.received().hideHeaderBackButton();
    });

    it('should show exit button', () => {
      mockLayoutService.received().showHeaderExitButton();
    });
  });
});