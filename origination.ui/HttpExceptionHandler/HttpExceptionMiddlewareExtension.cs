using Microsoft.AspNetCore.Builder;

namespace origination.ui.HttpExceptionHandler
{
    public static class HttpExceptionMiddlewareExtension
    {
        public static void UseCustomExceptionMiddlewareHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<HttpExceptionMiddleware>();
        }
    }
}