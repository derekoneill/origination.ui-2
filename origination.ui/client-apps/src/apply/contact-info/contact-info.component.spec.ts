import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { AnalyticsService } from '@ua/shared/analytics';
import {
  Address,
  AddressApiService, ApiZipLookupDto, Application, ApplicationState,
  ApplyPageKey,
  CustomerState,
  LocationService
} from '@ua/shared/data-access';
import { cityValidator, stateValidator, street1Validator, zipValidator } from '@ua/shared/utilities';
import { chance } from 'jest-chance';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { ContactInfoComponent } from './contact-info.component';
import { LayoutService } from '@ua/shared/ui';

describe('ContactInfoComponent', () => {
  let component: ContactInfoComponent;

  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockCustomerState: SubstituteOf<CustomerState>;
  let mockFormBuilder: SubstituteOf<FormBuilder>;
  let mockRouter: SubstituteOf<Router>;
  let mockLocationService: SubstituteOf<LocationService>;
  let mockTitle: SubstituteOf<Title>;
  let mockAddressApi: SubstituteOf<AddressApiService>;
  let mockLayoutService: SubstituteOf<LayoutService>;

  beforeEach(() => {
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockApplicationState = Substitute.for<ApplicationState>();
    mockCustomerState = Substitute.for<CustomerState>();
    mockTitle = Substitute.for<Title>();
    mockFormBuilder = Substitute.for<FormBuilder>();
    mockLocationService = Substitute.for<LocationService>();
    mockAddressApi = Substitute.for<AddressApiService>();
    mockTitle.getTitle().returns('mock Title');
    mockRouter = Substitute.for<Router>();
    mockRouter.url.returns('test Url');
    mockLayoutService = Substitute.for<LayoutService>();

    component = new ContactInfoComponent(
      mockApplyRouter,
      mockApplicationState,
      mockCustomerState,
      mockFormBuilder,
      mockLocationService,
      mockAddressApi,
      mockLayoutService
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('On Init', () => {
    let applicationSubject: BehaviorSubject<Application>;
    let addressSubject: BehaviorSubject<Address>;
    let mockFormGroup: SubstituteOf<FormGroup>;

    beforeEach(() => {
      applicationSubject = new BehaviorSubject<Application>({} as Application);
      addressSubject = new BehaviorSubject<Address>({} as Address);
      mockFormGroup = Substitute.for<FormGroup>();
      mockApplicationState.application$.returns(applicationSubject.asObservable());
      mockCustomerState.address$.returns(addressSubject.asObservable());
      mockFormBuilder.group(Arg.any()).returns(mockFormGroup);
    });

    it('should keep track of subscriptions', () => {
      expect(component.subs.length).toBe(0);
      component.ngOnInit();

      expect(component.subs.length).toBeGreaterThan(0);
    });

    it('should add validations for the street1 field', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.street1[1];
          return (
            validators.includes(Validators.required) &&
            validators.includes(street1Validator)
          );
        }),
        Arg.any()
      );
    });

    it('should add validations for the street2 field', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.street2[1];
          return validators.includes(street1Validator);
        }),
        Arg.any()
      );
    });

    it('should add validations for the city field', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.city[1];
          return (
            validators.includes(Validators.required) &&
            validators.includes(cityValidator)
          );
        }),
        Arg.any()
      );
    });

    it('should add validations for the state field', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.state[1];
          return (
            validators.includes(Validators.required) &&
            validators.includes(stateValidator)
          );
        }),
        Arg.any()
      );
    });

    it('should add validations for the zip field', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.zip[1];
          return (
            validators.includes(Validators.required) &&
            validators.includes(zipValidator)
          );
        }),
        Arg.any()
      );
    });

    it('should add validations for the state/zip fields with value', () => {
      addressSubject.next({ state: chance.state(), zip: chance.zip() } as Address);
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.state[1];
          return (
            validators.includes(Validators.required) &&
            validators.includes(stateValidator)
          );
        }),
        Arg.any()
      );
    });
  });

  describe('After view init', () => {
    let mockContactInfoFormGroup: SubstituteOf<FormGroup>;
    let mockCityFormControl: SubstituteOf<FormControl>;
    let mockStateFormControl: SubstituteOf<FormControl>;
    let mockZipFormControl: SubstituteOf<FormControl>;
    let mockStateSubject: Subject<string>;
    let mockZipSubject: Subject<string>;

    beforeEach(() => {
      mockContactInfoFormGroup = Substitute.for<FormGroup>();
      mockCityFormControl = Substitute.for<FormControl>();
      mockStateFormControl = Substitute.for<FormControl>();
      mockZipFormControl = Substitute.for<FormControl>();
      mockStateSubject = new Subject<string>();
      mockZipSubject = new Subject<string>();
      mockStateFormControl.valueChanges.returns(mockStateSubject.asObservable());
      mockZipFormControl.valueChanges.returns(mockZipSubject.asObservable());
      mockContactInfoFormGroup.get('city').returns(mockCityFormControl);
      mockContactInfoFormGroup.get('state').returns(mockStateFormControl);
      mockContactInfoFormGroup.get('zip').returns(mockZipFormControl);
      component.contactInfoFormGroup = mockContactInfoFormGroup;
    });

    it('should enable timout feature', () => {
      component.ngAfterViewInit();
      mockLayoutService.received().enableTimeout();
    });

    it('should remove any errors from zip form control when state form control is falsy', () => {
      component.ngAfterViewInit();
      mockStateSubject.next(undefined);
      mockZipFormControl.received().setErrors({});
    });

    it('should not remove any errors from zip form control when state form control is not falsy', () => {
      component.ngAfterViewInit();
      mockStateSubject.next(chance.zip());
      mockZipFormControl.didNotReceive().setErrors({});
    });

    it('should not update city/state by zip if zip is invalid', () => {
      component.ngAfterViewInit();
      mockZipSubject.next(chance.string({ pool: '456784-' }));
      mockAddressApi.didNotReceive().getCityAndStateByZip(Arg.any());
    });

    it('should not update city/state by zip city/state form controls already have a value', () => {
      const zip = chance.zip();
      mockCityFormControl.value.returns(chance.city());
      mockStateFormControl.value.returns(chance.state());

      component.ngAfterViewInit();
      mockZipSubject.next(zip);
      mockAddressApi.didNotReceive().getCityAndStateByZip(zip);
    });

    it('should update city/state by zip if zip is valid and state and city form controls have no value', () => {
      const city = chance.city();
      const state = chance.state();
      const zip = chance.zip();
      const apiLookup = {
        city,
        state,
        zip
      } as ApiZipLookupDto;
      const apiLookupSubject = new Subject<ApiZipLookupDto>();
      mockAddressApi.getCityAndStateByZip(zip).returns(apiLookupSubject.asObservable());
      mockCityFormControl.value.returns(undefined);
      mockStateFormControl.value.returns(undefined);
      mockZipFormControl.value.returns(zip);

      component.ngAfterViewInit();
      mockZipSubject.next(zip);
      apiLookupSubject.next(apiLookup);
      mockAddressApi.received().getCityAndStateByZip(zip);
    });

    it('should not update city by zip if zip is valid and city form control has values', () => {
      const city = chance.city();
      const state = chance.state();
      const zip = chance.zip();
      const apiLookup = {
        city,
        state,
        zip
      } as ApiZipLookupDto;
      const apiLookupSubject = new Subject<ApiZipLookupDto>();
      mockAddressApi.getCityAndStateByZip(zip).returns(apiLookupSubject.asObservable());
      mockCityFormControl.value.returns(chance.city());
      mockStateFormControl.value.returns(undefined);
      mockZipFormControl.value.returns(zip);

      component.ngAfterViewInit();
      mockZipSubject.next(zip);
      apiLookupSubject.next(apiLookup);
      mockStateFormControl.received().setValue(state);
    });

    it('should not update state by zip if zip is valid and state form control has values', () => {
      const city = chance.city();
      const state = chance.state();
      const zip = chance.zip();
      const apiLookup = {
        city,
        state,
        zip
      } as ApiZipLookupDto;
      const apiLookupSubject = new Subject<ApiZipLookupDto>();
      mockAddressApi.getCityAndStateByZip(zip).returns(apiLookupSubject.asObservable());
      mockCityFormControl.value.returns(undefined);
      mockStateFormControl.value.returns(chance.state());
      mockZipFormControl.value.returns(zip);

      component.ngAfterViewInit();
      mockZipSubject.next(zip);
      apiLookupSubject.next(apiLookup);
      mockCityFormControl.received().patchValue(city);
    });
  });

  describe('On Destroy', () => {
    let mockSubscription: SubstituteOf<Subscription>;

    beforeEach(() => {
      const observable = Substitute.for<Observable<any>>();
      mockSubscription = Substitute.for<Subscription>();
      observable.subscribe().returns(mockSubscription);
      component.subs.push(observable.subscribe());
    });

    it('should unsubscribe the open subs', () => {
      component.ngOnDestroy();

      mockSubscription.received().unsubscribe();
    });
  });

  describe('getZipStateMismatchError', () => {
    let mockContactInfoFormGroup: SubstituteOf<FormGroup>;
    let mockStateFormControl: SubstituteOf<FormControl>;
    let mockZipFormControl: SubstituteOf<FormControl>;
    let state: string;
    let zip: string;
    let randomPrefix: string;
    let randomPostfix: string;
    let baseError: string;

    beforeEach(() => {
      mockContactInfoFormGroup = Substitute.for<FormGroup>();
      mockStateFormControl = Substitute.for<FormControl>();
      mockZipFormControl = Substitute.for<FormControl>();
      state = chance.string();
      zip = chance.string();
      mockStateFormControl.value.returns(state);
      mockZipFormControl.value.returns(zip);
      randomPrefix = chance.string();
      randomPostfix = chance.string();
      baseError = `${randomPrefix}{{ zip }}{{ state }}${randomPostfix}`;
      mockContactInfoFormGroup.get('state').returns(mockStateFormControl);
      mockContactInfoFormGroup.get('zip').returns(mockZipFormControl);
      component.contactInfoFormGroup = mockContactInfoFormGroup;
    });

    it('should do nothing without a base error', () => {
      const expected = undefined;
      const actual = component.getZipStateMismatchError('');

      expect(actual).toBe(expected);
    });

    it('should create a zip state mismatch error using available zip and state', () => {
      const expected = `${randomPrefix}${zip}${state}${randomPostfix}`;
      const actual = component.getZipStateMismatchError(baseError);

      expect(actual).toBe(expected);
    });
  });

  describe('On Continue', () => {
    let contactInfoFormGroup: SubstituteOf<FormGroup>;

    beforeEach(() => {
      contactInfoFormGroup = Substitute.for<FormGroup>();
    });

    it('should do nothing if the contact form group is invalid', () => {
      contactInfoFormGroup.valid.returns(false);
      component.contactInfoFormGroup = contactInfoFormGroup;
      component.onContinue();
      mockCustomerState.didNotReceive().updateAddress(Arg.any());
      mockApplyRouter.didNotReceive().stepForward();
    });

    describe('if the contact form group is valid', () => {
      let mockContactInfoFormGroup: SubstituteOf<FormGroup>;

      beforeEach(() => {
        mockContactInfoFormGroup = Substitute.for<FormGroup>();
        mockContactInfoFormGroup.valid.returns(true);
        component.contactInfoFormGroup = mockContactInfoFormGroup;
      });

      it('should update the application address', () => {
        component.onContinue();
        mockCustomerState.received().updateAddress(Arg.any());
      });

      it('should move to the next page if not in edit mode', () => {
        mockLayoutService.editMode.returns(false);
        component.onContinue();
        mockApplyRouter.received().stepForward();
      });

      it('should move to the review page if in edit mode', () => {
        mockLayoutService.editMode.returns(true);
        component.onContinue();
        mockApplyRouter.received().navigate(ApplyPageKey.review);
      });

    });
  });
});