import {
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';
import { LocationService } from '@ua/shared/data-access';
import { BehaviorSubject } from 'rxjs';
import { constants } from '../constants';

export const cityValidator = (c: FormControl) => {
  const value = c.value;
  const numberRegex = /[0-9]/;
  const specialCharacterRegex = /[\!\@\#\$\%\^\&\*\_\<\>,\\\/\+\=\'\"\(\)\{\}\[\]\~`\?|\;\:]/;
  if (value && numberRegex.test(value)) {
    return { number: true };
  }
  if (specialCharacterRegex.test(value)) {
    return { specialChar: true };
  }

  if (value && value[0] === ' ') {
    return {
      spaceFormat: true,
    };
  }

  return null;
};

export const validateStateZipMatch = (
  checkLoader: BehaviorSubject<boolean>,
  locationService: LocationService
): ValidatorFn => (control: FormGroup): ValidationErrors => {
  checkLoader.next(true);
  const state = control.get('state').value;
  let zip: string = control.get('zip').value;

  zip = zip.split('-')[0];

  if (state && state.length === 2 && zip) {
    locationService
      .validateStateAndZip({
        stateCode: state,
        zipCode: zip,
      })
      .subscribe(res => {
        if (res) {
          const errs = control.get('zip').errors || {};
          delete errs['zipStateMismatch'];
          control
            .get('zip')
            .updateValueAndValidity({ onlySelf: true, emitEvent: true });
        } else {
          control
            .get('zip')
            .setErrors({ zipStateMismatch: true }, { emitEvent: true });
        }
        checkLoader.next(false);
      });
  }
  return null;
};

export const stateValidator = (c: FormControl) => {
  const value = c.value;

  if (!value) {
    return;
  }

  const selectedValue = constants.stateSearch.filter(x => x.value === value);
  if (value[0] === ' ') {
    return {
      spaceFormat: true,
    };
  } else if (!selectedValue.length) {
    return {
      selection: true,
    };
  }
  return null;
};

export const zipValidator = (c: FormControl) => {
  const value = c.value;
  if (
    value &&
    !value.match(/^\d{5}$/) &&
    !value.match(/^\d{9}$/) &&
    !value.match(/^\d{5}-\d{4}$/)
  ) {
    return {
      sixToEightDigits: true,
    };
  }
  return null;
};

export const street1Validator = (c: FormControl) => {
  const value = c.value;
  if (value) {
    if (
      value.match(
        /\bP(ost|ostal)?([ \.]*(O|0)(ffice)?)?[ \.]*((B)([o,x])*)?\b/gi
      )
    ) {
      return {
        poBox: true,
      };
    } else if (!value.match(/^[\s\da-zA-Z.,\-&#\/']*$/gi)) {
      return {
        formatting: true,
      };
    } else if (value[0] === ' ') {
      return {
        spaceFormat: true,
      };
    }
  }
  return null;
};

export const street2Validator = () => (c: FormControl) => {
  const value = c.value;

  if (value && value[0] === ' ') {
    return {
      spaceFormat: true,
    };
  }
  return null;
};