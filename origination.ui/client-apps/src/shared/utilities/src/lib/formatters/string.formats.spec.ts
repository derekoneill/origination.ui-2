import { formatFormDate } from '@ua/shared/utilities';

describe('String formatter', () => {
  describe('Format Form Date', () => {
    it('Formats string date', () => {
      const testDate = '1/2/90';
      const expectedDate = '01/02/1990';

      const res = formatFormDate(testDate);
      expect(res).toBe(expectedDate);
    });

    it('Formats date object', () => {
      const testDate = new Date('1990-1-2');

      const expectedDate = '01/02/1990';

      const res = formatFormDate(testDate);
      expect(res).toBe(expectedDate);
    });

    it('Handles null date', () => {
      const res = formatFormDate(null);
      expect(res).toBeNull();
    });

    it('Handle bad year string', () => {
      const testDate = '1/2/ab';

      const res = formatFormDate(testDate);
      expect(res).toBeNull();
    });
  });
});