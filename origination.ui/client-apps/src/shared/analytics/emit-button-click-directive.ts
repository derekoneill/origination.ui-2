import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { ClickEventType } from './analytics-event-emission-enums';
import { ClickEventProperties } from './analytics-event-emission-interfaces';
import { AnalyticsService } from './analytics.service';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'button, [emitButtonClick], grit-wc-button',
})
export class EmitButtonClickDirective {
  @Input('emitButtonClick') clickEventProperties: ClickEventProperties;

  constructor(
    private elementRef: ElementRef,
    private analyticsService: AnalyticsService
  ) { }

  @HostListener('click') onClick(): void {
    const clickEventLabel: string =
      this.clickEventProperties &&
        this.clickEventProperties.eventProperties &&
        this.clickEventProperties.eventProperties.eventLabel
        ? this.clickEventProperties.eventProperties.eventLabel
        : this.elementRef.nativeElement.innerText;
    const clickEventValue: number =
      this.clickEventProperties &&
        this.clickEventProperties.eventProperties &&
        this.clickEventProperties.eventProperties.eventValue
        ? this.clickEventProperties.eventProperties.eventValue
        : 0;
    const clickEventType: ClickEventType =
      this.clickEventProperties && this.clickEventProperties.eventType
        ? this.clickEventProperties.eventType
        : ClickEventType.button;
    this.analyticsService.trackUniversalButtonClick({
      eventProperties: {
        eventLabel: clickEventLabel,
        eventValue: clickEventValue,
      },
      type: clickEventType,
    });
  }
}