import { ElementRef } from '@angular/core';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { FormElementEngagementEventType } from './analytics-event-emission-enums';
import { FormElementEmission, FormElementEventProperties } from './analytics-event-emission-interfaces';
import { AnalyticsService } from './analytics.service';
import { EmitFormElementEngagementDirective } from './emit-form-element-engagement.directive';

describe('EmitFormElementEngagementDirective', () => {
    let directive: EmitFormElementEngagementDirective;
    let mockElementRef: SubstituteOf<ElementRef>;
    let mockAnalyticsService: SubstituteOf<AnalyticsService>;
    let innerText: string;
    let id: string;
    let tagName: string;
    let value: string;

    describe('Given a element that is not an input', () => {
        beforeEach(() => {
            mockElementRef = Substitute.for<ElementRef>();
            mockAnalyticsService = Substitute.for<AnalyticsService>();
            innerText = chance.string();
            id = chance.string();
            tagName = 'LABEL';
            value = chance.string();
            mockElementRef.nativeElement.returns({
                innerText,
                id,
                tagName,
                value
            });

            directive = new EmitFormElementEngagementDirective(mockElementRef, mockAnalyticsService);
        });

        it('should be created', () => {
            expect(directive).toBeDefined();
        });

        it('should not fire a form fill engagement analytics event if blurring on non input element', () => {
            directive.onBlur();

            mockAnalyticsService.didNotReceive().trackFormFill(Arg.any());
        });

        it('should fire a default form element engagement analytics event if not clicking on an input directly', () => {
            const expected: FormElementEmission = {
                eventProperties: {
                    eventLabel: innerText,
                    eventValue: 0
                },
                type: FormElementEngagementEventType.formElement
            };

            directive.onClick();
            mockAnalyticsService.received().trackFormElementEngagement(expected);
        });

        it('should fire a form element engagement analytics event with overrides', () => {
            const eventLabel = chance.string();
            const eventValue = chance.integer({ min: 0, max: 1000 });
            const type = chance.pickone([
                FormElementEngagementEventType.formElement,
                FormElementEngagementEventType.formFill
            ]);
            const mockFormElementEngagementProperties: FormElementEventProperties = {
                eventProperties: {
                    eventLabel,
                    eventValue
                },
                type
            };
            const expected: FormElementEmission = {
                eventProperties: {
                    eventLabel,
                    eventValue
                },
                type
            };

            directive.formElementEngagementProperties = mockFormElementEngagementProperties;
            directive.onClick();
            mockAnalyticsService.received().trackFormElementEngagement(expected);
        });
    });

    describe('Given an Input element', () => {
        beforeEach(() => {
            mockElementRef = Substitute.for<ElementRef>();
            mockAnalyticsService = Substitute.for<AnalyticsService>();
            innerText = chance.string();
            id = chance.string();
            tagName = 'INPUT';
            value = chance.string();
            mockElementRef.nativeElement.returns({
                innerText,
                id,
                tagName,
                value
            });

            directive = new EmitFormElementEngagementDirective(mockElementRef, mockAnalyticsService);
        });

        it('should not fire a form element engagement analytics event if clicking on an input directly', () => {
            directive.onClick();

            mockAnalyticsService.didNotReceive().trackFormElementEngagement(Arg.any());
        });

        it('should fire a default form fill engagement analytics event if blurring from a filled in input', () => {
            const expected: FormElementEmission = {
                eventProperties: {
                    eventLabel: id,
                    eventValue: 0
                },
                type: FormElementEngagementEventType.formElement
            };

            directive.onBlur();
            mockAnalyticsService.received().trackFormFill(expected);
        });

        it('should fire a form fill engagement analytics event with overrides if blurring from a filled in input', () => {
            const eventLabel = chance.string();
            const eventValue = chance.integer({ min: 0, max: 1000 });
            const type = chance.pickone([
                FormElementEngagementEventType.formElement,
                FormElementEngagementEventType.formFill
            ]);
            const mockFormElementEngagementProperties: FormElementEventProperties = {
                eventProperties: {
                    eventLabel,
                    eventValue
                },
                type
            };
            const expected: FormElementEmission = {
                eventProperties: {
                    eventLabel,
                    eventValue
                },
                type
            };

            directive.formElementEngagementProperties = mockFormElementEngagementProperties;
            directive.onBlur();
            mockAnalyticsService.received().trackFormFill(expected);
        });

    });

    describe('Given an Input element with an empty value attribute', () => {
        beforeEach(() => {
            mockElementRef = Substitute.for<ElementRef>();
            mockAnalyticsService = Substitute.for<AnalyticsService>();
            innerText = chance.string();
            id = chance.string();
            tagName = 'INPUT';
            mockElementRef.nativeElement.returns({
                innerText,
                id,
                tagName,
            });

            directive = new EmitFormElementEngagementDirective(mockElementRef, mockAnalyticsService);
        });

        it('should not fire a form fill engagement analytics event if blurring on unfilled input element', () => {
            directive.onBlur();

            mockAnalyticsService.didNotReceive().trackFormFill(Arg.any());
        });
    });

});