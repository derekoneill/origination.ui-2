import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import {
  ApplicationPayFrequency,
  ApplyPageKey,
  CreditCardInformation,
  Customer,
  CustomerState,
} from '@ua/shared/data-access';
import { chance } from 'jest-chance';
import { Observable, Subject, Subscription } from 'rxjs';
import { ReviewComponent } from './review.component';
import { LayoutService } from 'src/shared/ui/src/lib/layout.service';
import { ApplyRouter } from '../apply-router';

describe('ReviewComponent', () => {
  let component: ReviewComponent;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockCustomerState: SubstituteOf<CustomerState>;
  let mockApplyRouter: SubstituteOf<ApplyRouter>;

  beforeEach(() => {
    mockLayoutService = Substitute.for<LayoutService>();
    mockCustomerState = Substitute.for<CustomerState>();
    mockApplyRouter = Substitute.for<ApplyRouter>();
    component = new ReviewComponent(
      mockLayoutService,
      mockCustomerState,
      mockApplyRouter,
    );
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('On init', () => {
    let customerSubject: Subject<Customer>;
    let cardSubject: Subject<CreditCardInformation>;

    beforeEach(() => {
      customerSubject = new Subject<Customer>();
      cardSubject = new Subject<CreditCardInformation>();
      mockCustomerState.customer$.returns(customerSubject.asObservable());
      mockCustomerState.card$.returns(cardSubject.asObservable());
    });

    it('should set the customer', () => {
      const actual = {
        incomeSource: {
          payFrequency: ApplicationPayFrequency.twicePerMonth
        }
      } as Customer;

      component.ngOnInit();
      customerSubject.next(actual);
      expect(component.customer).toBe(actual);
    });

    it('should set the card', () => {
      const actual = {} as CreditCardInformation;

      component.ngOnInit();
      cardSubject.next(actual);
      expect(component.card).toBe(actual);
    });

    it('should hide footer', () => {
      component.ngOnInit();
      mockLayoutService.received().hideFooter();
    });

    it('should show back button', () => {
      component.ngOnInit();
      mockLayoutService.received().showHeaderBackButton();
    });

    it('should show exit button', () => {
      component.ngOnInit();
      mockLayoutService.received().showHeaderExitButton();
    });
  });

  describe('Subscription cleanup', () => {
    let mockSubscription: SubstituteOf<Subscription>;

    beforeEach(() => {
      const observable = Substitute.for<Observable<any>>();
      mockSubscription = Substitute.for<Subscription>();
      observable.subscribe().returns(mockSubscription);
      component.subs.push(observable.subscribe());
    });

    it('should unsubscribe the open subs', () => {
      mockSubscription.closed.returns(false);
      component.ngOnDestroy();

      mockSubscription.received().unsubscribe();
    });

    it('should not unsubscribe the closed subs', () => {
      mockSubscription.closed.returns(true);
      component.ngOnDestroy();

      mockSubscription.didNotReceive().unsubscribe();
    });
  });

  describe('onContinue', () => {
    it('should step forward', () => {
      component.onContinue();
      mockApplyRouter.received().stepForward();
    });
  });

  describe('Update Information', () => {
    const pageKey = chance.pickone(ApplyPageKey);
    it('should enable edit mode', () => {
      component.updateInformation(pageKey);
      mockLayoutService.received().enableEditMode();
    });
    it('should navigate to given page', () => {
      component.updateInformation(pageKey);
      mockApplyRouter.received().navigate(pageKey);
    });
  });

  describe('isObjectEmpty', () => {
    describe('Given an empty object', () => {
      const obj = {};

      it('should return true', () => {
        expect(component.isObjectEmpty(obj)).toBe(true);
      });
    });
    describe('Given a non-empty object', () => {
      const obj = {
        [chance.string()]: chance.string()
      };

      it('should return false', () => {
        expect(component.isObjectEmpty(obj)).toBe(false);
      });
    });
  });

});