import { Injectable } from '@angular/core';
import { deepMerge } from '@ua/shared/utilities';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import {
  Application,
  Decision,
  DecisionStatus
} from './application.dto';
import { ApplicationService } from './application.service';

@Injectable()
export class ApplicationState {
  private readonly stateSubject = new BehaviorSubject<Application>({} as Application);
  private readonly decisionSubject = new ReplaySubject<Decision>();

  public constructor(private readonly applicationService: ApplicationService) { }

  public updateApplication(applicationUpdates: Application) {
    const updatedApplication = deepMerge(
      this.getApplicationSnapshot(),
      applicationUpdates
    );
    this.stateSubject.next(updatedApplication);
  }

  public prefillApplication(customerId: number, storeId: number, lastFour: string) {
    this.applicationService.requestPrefill(customerId, storeId, lastFour).subscribe(app => {
      this.updateApplication({
        storeId: app.storeId,
        customerId: app.customerId
      } as Application);
    });
  }


  public submitApplication() {
    this.applicationService
      .submitApplication(this.getApplicationSnapshot())
      .subscribe(decision => {
        this.decisionSubject.next(decision);
      });
  }

  public get application$(): Observable<Application> {
    return this.stateSubject.asObservable();
  }

  public get decision$(): Observable<Decision> {
    return this.decisionSubject.asObservable().pipe(filter(x => !!x));
  }

  public get decisionStatus$(): Observable<DecisionStatus> {
    return this.decision$.pipe(map(decision => decision.status));
  }

  private getApplicationSnapshot(): Application {
    return this.stateSubject.value;
  }
}