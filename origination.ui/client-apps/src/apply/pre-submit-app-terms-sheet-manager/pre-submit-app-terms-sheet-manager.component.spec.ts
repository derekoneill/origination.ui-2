import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import {
  Application,
  ApplicationState,
} from '@ua/shared/data-access';
import { Subject } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { PreSubmitAppTermsSheetManagerComponent } from './pre-submit-app-terms-sheet-manager.component';
import { LayoutService } from '@ua/shared/ui';

describe('TermssheetManager', () => {
  let component: PreSubmitAppTermsSheetManagerComponent;

  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockLayoutService: SubstituteOf<LayoutService>;

  beforeEach(() => {
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockApplicationState = Substitute.for<ApplicationState>();
    mockLayoutService = Substitute.for<LayoutService>();

    component = new PreSubmitAppTermsSheetManagerComponent(
      mockApplyRouter,
      mockApplicationState,
      mockLayoutService
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('On Init', () => {
    let applicationSubject: Subject<Application>;

    beforeEach(() => {
      applicationSubject = new Subject<Application>();
      mockApplicationState.application$.returns(
        applicationSubject.asObservable()
      );
    });

    it('should enable timout feature', () => {
      component.ngAfterViewInit();
      mockLayoutService.received().enableTimeout();
    });

    it('should fetch the application for the analytics', () => {
      const application = {} as Application;
      component.ngOnInit();
      applicationSubject.next(application);

      expect(component.application).toBe(application);
    });
  });

  describe('On pre-submit-app term sheet', () => {
    it('should dispatch to the next step in the apply flow route', () => {
      component.onContinue();

      mockApplyRouter.received().stepForward();
    });
  });
});