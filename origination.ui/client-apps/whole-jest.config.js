module.exports = {
  name: 'whole-project',
  setupFilesAfterEnv: ['<rootDir>/test-setup.ts'],
  globals: {
    'ts-jest': {
      tsConfig: '<rootDir>/tsconfig.spec.json',
      stringifyContentPathRegex: '\\.(html|svg)$',
      astTransformers: [
        'jest-preset-angular/build/InlineFilesTransformer',
        'jest-preset-angular/build/StripStylesTransformer',
      ],
    },
  },
  coverageDirectory: '<rootDir>/coverage/src/shared/whole-project',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
  coverageThreshold: {
    global: {
      branches: 30,
      functions: 50,
      lines: 65,
      statements: 65,
    },
  },

  testMatch: ['**/+(*.)+(spec|test).+(ts|js)?(x)', '!**/*e2e/**'],
  transform: {
    '^.+\\.(ts|js|html)$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'js', 'html'],
  moduleNameMapper: {    
      "@ua/shared/data-access": "<rootDir>/src/shared/data-access/src/index.ts",
      "@ua/apply": "<rootDir>/src/apply/index.ts",
      "@ua/shared/utilities": "<rootDir>/src/shared/utilities/src/index.ts",
      "@ua/shared/ui": "<rootDir>/src/shared/ui/src/index.ts",
      "@ua/shared/analytics": "<rootDir>/src/shared/analytics/index.ts"
  },
  collectCoverage: false,
  coverageReporters: ['text', 'text-summary', 'html', 'lcov'],
  globalSetup: 'jest-chance',
};
