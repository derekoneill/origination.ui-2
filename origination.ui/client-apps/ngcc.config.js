module.exports = {
  packages: {
    '@progleasing/grit-universal-angular': {
      ignorableDeepImportMatchers: [/grit-universal\//],
    },
    'angular2-text-mask': {
      ignorableDeepImportMatchers: [/text-mask-core\//],
    },
  },
};
