import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { ClickEventType } from './analytics-event-emission-enums';
import { AnalyticsEventProperties } from './analytics-event-emission-interfaces';
import { AnalyticsService } from './analytics.service';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'a, [emitLinkClick]',
})
export class EmitLinkClickDirective {
  @Input('emitLinkClick') clickEventProperties: AnalyticsEventProperties;

  constructor(
    private elementRef: ElementRef,
    private analyticsService: AnalyticsService
  ) {}

  @HostListener('click') onClick(): void {
    const clickEventLabel: string =
      this.clickEventProperties && this.clickEventProperties.eventLabel
        ? this.clickEventProperties.eventLabel
        : this.elementRef.nativeElement.innerText;
    const clickEventValue: number =
      this.clickEventProperties && this.clickEventProperties.eventValue
        ? this.clickEventProperties.eventValue
        : 0;
    this.analyticsService.trackUniversalLinkClick({
      eventProperties: {
        eventLabel: clickEventLabel,
        eventValue: clickEventValue,
      },
      type: ClickEventType.link,
    });
  }
}