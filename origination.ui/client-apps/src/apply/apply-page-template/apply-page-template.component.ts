import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApplyPageKey } from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { Observable } from 'rxjs';

@Component({
  selector: 'ua-apply-page-template',
  templateUrl: './apply-page-template.component.html',
  styleUrls: ['./apply-page-template.component.scss'],
})
export class ApplyPageTemplateComponent implements OnInit {
  @Input()
  public formGroup: FormGroup;
  @Input()
  public pageRoute: ApplyPageKey;
  @Output() continue = new EventEmitter<void>();

  public editMode: boolean;
  public editModeButtonText$: Observable<string>;
  public loading = false;

  constructor(
    private readonly layoutService: LayoutService,
  ) { }

  ngOnInit() {
    this.editMode = this.layoutService.editMode;
    this.layoutService.showHeaderBackButton();
    this.layoutService.showHeaderExitButton();
    this.layoutService.hideFooter();

    this.formGroup.statusChanges.subscribe(newStatus => this.updateStatus(newStatus));

    this.touchPopulatedFormControls();
  }

  onContinue() {
    this.loading = true;
    this.continue.emit();
  }

  updateStatus(newStatus: string): void {
    if (newStatus === 'INVALID') {
      this.loading = false;
    }
  }

  private touchPopulatedFormControls() {
    const formControls = Object.keys(this.formGroup.controls);
    for (const formControl in formControls) {
      if (this.formGroup.controls[formControls[formControl]].value) {
        this.formGroup.controls[formControls[formControl]].markAsTouched();
      }
    }
  }
}