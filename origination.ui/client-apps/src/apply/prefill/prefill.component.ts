/* eslint-disable @angular-eslint/component-selector */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  ApplyPageKey,
  ApplicationState,
  Customer, CustomerState,
  OnboardingState
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { Subscription, combineLatest } from 'rxjs';
import { ApplyRouter } from '../apply-router';

@Component({
  selector: 'prefill',
  templateUrl: './prefill.component.html',
  styleUrls: ['./prefill.component.scss'],
})
export class PrefillComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  pageRoute: ApplyPageKey = ApplyPageKey.prefill;

  prefillForm: FormGroup;

  public requestFillData = true;

  private customerId: number;
  private storeId: number;

  constructor(
    private formBuilder: FormBuilder,
    private readonly onboardingState: OnboardingState,
    private readonly applicationState: ApplicationState,
    private readonly customerState: CustomerState,
    private readonly layoutService: LayoutService,
    private readonly applyRouter: ApplyRouter
  ) { }

  ngOnInit(): void {
    this.initForm();

    this.subs.push(
      combineLatest([
        this.customerState.customer$,
        this.onboardingState.onboardingInfo$
      ]).subscribe(([customer, info]) => this.setPrefillData(customer.customerId, info.storeId)));
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => !s.closed && s.unsubscribe());
  }

  public onContinue(): void {
    if (this.prefillForm.valid) {
      this.applicationState.prefillApplication(this.customerId, this.storeId, '');
      if (!this.layoutService.editMode) {
        this.applyRouter.stepForward();
      } else {
        this.applyRouter.navigate(ApplyPageKey.review);
      }
    }
  }

  private initForm(): void {
    this.prefillForm = this.formBuilder.group({ lastFour: ['', [Validators.required, Validators.minLength(4)]] });
  }

  private setPrefillData(customerId: number, storeId: number) {
    if (!this.requestFillData) {
      this.applicationState.prefillApplication(customerId, storeId, '');
      this.applyRouter.stepForward();
    }
    else {
      this.customerId = customerId;
      this.storeId = storeId;
    }
  }
}