import { Location } from '@angular/common';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  ParamMap,
  Router
} from '@angular/router';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import {
  AppConfig,
  ApplicationPayFrequency, ApplyPageKey,
  Customer, CustomerState, OnboardingState,
  ServiceConfigs
} from '@ua/shared/data-access';
import { chance } from 'jest-chance';
import { ReplaySubject, Subject } from 'rxjs';
import { ApplyRouter, routesOrder } from './apply-router';

describe('Apply Router', () => {
  let router: ApplyRouter;

  let mockAngularRouter: SubstituteOf<Router>;
  let mockActivatedRoute: SubstituteOf<ActivatedRoute>;
  let mockConfig: SubstituteOf<AppConfig>;
  let mockLocation: SubstituteOf<Location>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;
  let mockCustomerState: SubstituteOf<CustomerState>;

  beforeEach(() => {
    mockAngularRouter = Substitute.for<Router>();
    mockActivatedRoute = Substitute.for<ActivatedRoute>();
    mockConfig = Substitute.for<AppConfig>();
    mockLocation = Substitute.for<Location>();
    mockOnboardingState = Substitute.for<OnboardingState>();
    mockCustomerState = Substitute.for<CustomerState>();

    router = new ApplyRouter(
      mockActivatedRoute,
      mockAngularRouter,
      mockConfig,
      mockLocation,
      mockOnboardingState,
      mockCustomerState
    );
  });

  it('should create', () => {
    expect(router).toBeDefined();
  });

  describe('Internal Routing', () => {
    describe('Step Back', () => {
      it('should just use the browser back functionality', () => {
        router.stepBack();

        mockLocation.received().back();
      });
    });

    describe('Step Forward', () => {
      let currentPage: ApplyPageKey;

      describe('given a page that can move next', () => {
        beforeEach(() => {
          currentPage = chance.pickone([
            ApplyPageKey.contactInfo,
            ApplyPageKey.incomeInfo,
            ApplyPageKey.bankInfo,
            ApplyPageKey.initialPayment,
            ApplyPageKey.submitApplicationTerms,
            ApplyPageKey.submittedApplication,
          ]);
          mockAngularRouter.url.returns(`/apply/${currentPage}`);
        });

        it('should go to the next logical page', () => {
          const nextPage = routesOrder[currentPage].nextRoute;

          router.stepForward();

          mockAngularRouter
            .received()
            .navigate([`/${nextPage}`], Arg.any());
        });
      });

      describe('given a page that can move next with query strings', () => {
        beforeEach(() => {
          currentPage = chance.pickone([
            ApplyPageKey.contactInfo,
            ApplyPageKey.incomeInfo,
            ApplyPageKey.bankInfo,
            ApplyPageKey.initialPayment,
            ApplyPageKey.submitApplicationTerms,
            ApplyPageKey.submittedApplication,
          ]);
          mockAngularRouter.url.returns(
            `/${currentPage}?this=that&tit=tat`
          );
        });

        it('should go to the next logical page', () => {
          const nextPage = routesOrder[currentPage].nextRoute;

          router.stepForward();

          mockAngularRouter
            .received()
            .navigate([`/${nextPage}`], Arg.any());
        });
      });

      describe('given a page that should not move next', () => {
        beforeEach(() => {
          currentPage = ApplyPageKey.results;
          mockAngularRouter.url.returns(`/${currentPage}`);
        });

        it('should throw an exception', () => {
          expect(() => router.stepForward()).toThrowError();

          mockAngularRouter.didNotReceive().navigate(Arg.any(), Arg.any());
        });
      });
    });

    describe('Navigate', () => {
      let page: ApplyPageKey;

      beforeEach(() => {
        const allPages = Object.keys(ApplyPageKey).map(
          key => ApplyPageKey[key]
        );
        page = chance.pickone(allPages);
      });

      it('should route to the page', () => {
        router.navigate(page);

        mockAngularRouter.received().navigate([`/${page}`], Arg.any());
      });

      it('should use a merge parameter strategy', () => {
        router.navigate(page, {});

        mockAngularRouter.received().navigate(Arg.any(), {
          queryParams: {},
          queryParamsHandling: 'merge',
        });
      });
    });
  });

  describe('Restart Application', () => {
    it('should route to the root', () => {
      router.restartApplication();

      mockAngularRouter.navigate([`/`], Arg.any());
    });

    it('should use a merge parameter strategy', () => {
      router.restartApplication();

      mockAngularRouter.navigate(Arg.any(), { queryParamsHandling: 'merge' });
    });
  });

  describe('Navigate to users first incomplete page', () => {
    it('should go to contact info page when nothing is filled out', () => {
      const customerSubject = new Subject<Customer>();
      const customer = {} as Customer;
      mockCustomerState.customer$.returns(customerSubject.asObservable());
      router.navToFirstIncompletePage();
      customerSubject.next(customer);

      mockAngularRouter.received().navigate(['/home-address'], Arg.any());
    });

    it('should skip contact info page when address info provided', () => {
      const customerSubject = new Subject<Customer>();
      const customer = { address: { address1: 'test', address2: 'test2', city: 'nowhere', state: 'UT', zip: '84045' } } as Customer;
      mockCustomerState.customer$.returns(customerSubject.asObservable());
      router.navToFirstIncompletePage();
      customerSubject.next(customer);

      mockAngularRouter.received().navigate(['/income-info'], Arg.any());
    });

    it('should not skip contact info page when address info provided but is empty', () => {
      const customerSubject = new Subject<Customer>();
      const customer = { address: { address1: '', address2: '', city: '', state: '', zip: '' } } as Customer;
      mockCustomerState.customer$.returns(customerSubject.asObservable());
      router.navToFirstIncompletePage();
      customerSubject.next(customer);

      mockAngularRouter.received().navigate(['/home-address'], Arg.any());
    });

    it('should skip to bank info page when address and income info provided', () => {
      const customerSubject = new Subject<Customer>();
      const customer = {
        address: { address1: 'test', address2: 'test2', city: 'nowhere', state: 'UT', zip: '84045' },
        incomeSource: {
          monthlyGrossIncome: 5,
          lastPaymentDate: '01/02/2016',
          nextPaymentDate: '01/05/2016',
          payFrequency: ApplicationPayFrequency.weekly
        },
      } as Customer;
      mockCustomerState.customer$.returns(customerSubject.asObservable());
      router.navToFirstIncompletePage();
      customerSubject.next(customer);

      mockAngularRouter.received().navigate(['/bank-info'], Arg.any());
    });

    it('should skip to initial payment page when address and income info provided', () => {
      const customerSubject = new Subject<Customer>();
      const customer = {
        bank: { routingNumber: '1234', accountNumber: '1234' },
        address: { address1: 'test', address2: 'test2', city: 'nowhere', state: 'UT', zip: '84045' },
        incomeSource: {
          monthlyGrossIncome: 5,
          lastPaymentDate: '01/02/2016',
          nextPaymentDate: '01/05/2016',
          payFrequency: ApplicationPayFrequency.weekly
        },
      } as Customer;
      mockCustomerState.customer$.returns(customerSubject.asObservable());
      router.navToFirstIncompletePage();
      customerSubject.next(customer);

      mockAngularRouter.received().navigate(['/card-info'], Arg.any());
    });
  });

  describe('External Routing', () => {
    let mockWindow: SubstituteOf<Window>;

    beforeEach(() => {
      mockWindow = Substitute.for<Window>();
      //  We definitely do not want to wipe out window for the tests so we set the static readonly this way.
      //  This isn't a pattern we want to repeat for other mocks.
      (ApplyRouter as any).window = mockWindow;
    });

    describe('External Navigate', () => {
      it('should route to the url', () => {
        const url = chance.url();

        router.externalNavigate(url);

        mockWindow.received().href = url;
      });
    });

    describe('Forward to Auth', () => {
      const onboardingSubject = new ReplaySubject<string>();

      beforeEach(() => {
        mockOnboardingState.onboardingGuid$.returns(
          onboardingSubject.asObservable()
        );
      });

      it('should redirect to the auth app', () => {
        const auth = chance.url();
        const progApplyClientUrl = chance.url();
        let onboardingGuid: string;

        mockConfig.serviceConfigs.returns({
          auth,
          ui: progApplyClientUrl,
        } as ServiceConfigs);

        mockOnboardingState.onboardingGuid$.subscribe(guid => (onboardingGuid = guid));

        onboardingSubject.next(chance.string());

        router.forwardToAuth();

        const onSuccessUrl = `&onSuccess=${progApplyClientUrl}/${ApplyPageKey.termsAndConditions}`;
        const onCancelUrl = `&onCancel=${progApplyClientUrl}/${ApplyPageKey.landingPage}`;
        mockWindow.received().href = `${auth}/auth?onboarding=${onboardingGuid}${onSuccessUrl}${onCancelUrl}`;
      });
    });

    describe('getParam', () => {
      let mockRouteSnapshot: SubstituteOf<ActivatedRouteSnapshot>;
      let mockQueryParams: SubstituteOf<ParamMap>;
      let paramName: string;
      let param: string;

      beforeEach(() => {
        mockRouteSnapshot = Substitute.for<ActivatedRouteSnapshot>();
        mockQueryParams = Substitute.for<ParamMap>();
        paramName = chance.string();
        param = chance.string();
        mockQueryParams.get(paramName).returns(param);
        mockRouteSnapshot.queryParamMap.returns(mockQueryParams);
        mockActivatedRoute.snapshot.returns(mockRouteSnapshot);
      });

      it('should get the given route param', () => {
        expect(router.getParam(paramName)).toBe(param);
      });
    });
  });
});