import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { ClientEventBus } from '@progleasing/client-event-bus';
import {
  AppConfig,
  LeaseCostEstimatorPage,
  LeaseCostEstimatorRequest,
  LeaseEstimatesState,
  OnboardingState
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { combineLatest, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ApplyRouter } from '../apply-router';
import { LeaseCostEstimatorInjectable } from './lease-cost-estimator-injectable';

@Component({
  selector: 'ua-lease-cost-estimator-manager',
  templateUrl: './lease-cost-estimator-manager.component.html',
  styleUrls: ['./lease-cost-estimator-manager.component.scss'],
})
export class LeaseCostEstimatorManagerComponent
  implements OnInit, OnDestroy, AfterViewInit {
  static readonly bodyClass = 'lease-cost-estimator';
  static readonly hostName = 'Origination';

  @ViewChild('leaseCostEstimatorPortal', { static: false })
  public leaseCostEstimatorPortal: ElementRef;
  public subscriptions: Subscription;

  private clientEventBus: ClientEventBus;
  private renderPending: boolean;

  constructor(
    private readonly applyRouter: ApplyRouter,
    private readonly onboardingState: OnboardingState,
    private readonly leaseEstimatorState: LeaseEstimatesState,
    private readonly renderer: Renderer2,
    private readonly leaseCostEstimator: LeaseCostEstimatorInjectable,
    private readonly appConfig: AppConfig,
    private readonly layoutService: LayoutService,
    @Inject(DOCUMENT) private readonly document: Document
  ) { }

  public ngOnInit(): void {
    this.clientEventBus = this.leaseCostEstimator.getClientEventBus();
    this.eventHandling();
    this.renderer.addClass(
      this.document.body,
      LeaseCostEstimatorManagerComponent.bodyClass
    );
    this.layoutService.showHeaderBackButton();
    this.layoutService.showHeaderExitButton();
    this.layoutService.hideFooter();
  }

  public ngOnDestroy(): void {
    this.renderer.removeClass(
      this.document.body,
      LeaseCostEstimatorManagerComponent.bodyClass
    );
    if (this.subscriptions) { this.subscriptions.unsubscribe(); }
  }

  public ngAfterViewInit(): void {
    this.subscriptions =
      combineLatest([
        this.leaseEstimatorState.leaseCostEstimatorPage$,
        this.leaseEstimatorState.leaseCostEstimatorQuote$,
        this.onboardingState.onboardingInfo$,
      ])
        .pipe(
          filter(([viewingPage, quote, onboardingInfo]) => !!onboardingInfo?.storeId && !!onboardingInfo?.languageCode && !viewingPage)
        )
        .subscribe(([viewingPage, quote, onboardingInfo]) => {
          if (this.renderPending) {
            return;
          }

        this.render(
          onboardingInfo.storeId,
          onboardingInfo.languageCode,
          quote?.quoteRequest,
          onboardingInfo.storeLocation.address.state,
          viewingPage === LeaseCostEstimatorPage.results && !!quote?.quoteRequest
        );
      });
  }

  private async render(
    storeId: number,
    languageCode: string,
    quoteRequest: LeaseCostEstimatorRequest,
    state: string,
    immediateRequest?: boolean
  ) {
    if (!languageCode || !storeId) { return; }
    this.renderPending = true;
    this.leaseCostEstimatorPortal.nativeElement.innerHTML = '';
    const appLoadResult: any = await this.leaseCostEstimator.loadMicroFrontend(
      this.leaseCostEstimatorPortal.nativeElement
    );
    const appLoadResultInstance =
      appLoadResult.element.ngElementStrategy.componentRef.instance;

    if (!immediateRequest) {
      appLoadResultInstance.hasInitialized = await false;
      appLoadResult.initialize({
        paymentToolApiUrl: this.appConfig.serviceConfigs
          .leasePaymentEstimator,
        hostName: LeaseCostEstimatorManagerComponent.hostName,
        storeId,
        language: languageCode,
        quoteRequest,
        state,
      });
    } else {
      appLoadResultInstance.initializeResults({
        paymentToolApiUrl: this.appConfig.serviceConfigs
          .leasePaymentEstimator,
        hostName: LeaseCostEstimatorManagerComponent.hostName,
        storeId,
        language: languageCode,
        quoteRequest,
        state,
      });
    }
  }

  private eventHandling() {
    this.clientEventBus.resetAllListeners();
    this.clientEventBus.on('Lease.PaymentTool.createPaymentToolUI', () => {
      this.leaseEstimatorState.setLeaseCostViewingPage(
        LeaseCostEstimatorPage.tool
      );
      this.renderPending = false;
    });

    this.clientEventBus.on(
      'Lease.PaymentTool.appStateChange',
      onStateChange => {
        if (onStateChange.data === 'results') {
          this.leaseEstimatorState.setLeaseCostViewingPage(
            LeaseCostEstimatorPage.results
          );
        }
      }
    );

    this.clientEventBus.on(
      'Lease.PaymentTool.getEstimateSuccess',
      onSuccess => {
        this.leaseEstimatorState.setLeaseQuote(onSuccess.data);
      }
    );

    this.clientEventBus.on(
      'Lease.PaymentTool.destroyPaymentToolUI',
      onSuccess => {
        this.leaseEstimatorState.setLeaseCostViewingPage(null);
      }
    );

    this.clientEventBus.on('Lease.PaymentTool.getEstimateError', onError => {
      this.leaseEstimatorState.setLeaseQuoteError(onError.data);
    });

    this.clientEventBus.on('Lease.PaymentTool.continueButtonClicked', () => {
      this.applyRouter.stepForward();
    });
  }
}