import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  Application,
  ApplicationState,
  ApplyPageKey,
  CreditCardInformation,
  Customer,
  CustomerState,
  LocationService
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import {
  cityValidator,
  constants,
  creditCard,
  expirationDate,
  expirationDateTextMask,
  nameFormat,
  startsWithLetter,
  stateValidator,
  street1Validator,
  validateStateZipMatch,
  zipValidator
} from '@ua/shared/utilities';
import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ApplyRouter } from '../apply-router';

export enum InitialPaymentFormFields {
  firstNameOnCard = 'firstName',
  lastNameOnCard = 'lastName',
  cardNumber = 'cardNumber',
  cardExpiration = 'expDate',
  sameAsShipping = 'sameAsShipping',
  streetAddress1 = 'street1',
  streetAddress2 = 'street2',
  zipOnCard = 'zip',
  stateOnCard = 'state',
  cityOnCard = 'city',
  fakeCard = 'fakeCard'
}

@Component({
  selector: 'ua-initial-payment',
  templateUrl: './initial-payment.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InitialPaymentComponent
  implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('ccNum') set content(content: ElementRef) {
    if (content) { this.cardnumberElement = content; }
  }

  public subs: Subscription[] = [];
  public initialPaymentForm: FormGroup;
  public isMasked = true;
  public showFake = false;
  public ccError = false;
  public savedToken: string;
  public savedBin: string;
  public customer: Customer;
  public application: Application;
  public paymentCardControl$: Observable<any>;
  public addressControl$: Observable<any>;
  public sameAsShipping = true;
  public zipCheckLoadingSubject = new BehaviorSubject(false);
  public expDateTextMask = expirationDateTextMask;
  public stateOptions = constants.stateSearch;
  public pageRoute: ApplyPageKey = ApplyPageKey.initialPayment;

  private cardnumberElement: ElementRef;

  constructor(
    private readonly applyRouter: ApplyRouter,
    private readonly applicationState: ApplicationState,
    private readonly customerState: CustomerState,
    private readonly formBuilder: FormBuilder,
    private readonly locationService: LocationService,
    private readonly changeDetector: ChangeDetectorRef,
    private readonly layoutService: LayoutService,
  ) { }

  public ngOnInit(): void {
    this.isShippingAddressBillingAddress();
    this.initialPaymentForm = this.getInitialForm();

    this.subs.push(
      this.applicationState.application$.subscribe(
        application => (this.application = application)
      ),
      combineLatest([
        this.customerState.customer$,
        this.customerState.card$,
      ]).subscribe(([customer, card]) => this.updateCardInfo(customer, card))
    );
  }

  public creditCardConditionallyRequired(): Validators {
    return (c: FormControl) => {
      if (!this.initialPaymentForm) {
        return null;
      }
      if (this.showFake) { return null; }
      return Validators.required(c);
    };
  }

  public conditionallyRequired(): Validators {
    return (c: FormControl) => {
      if (!this.initialPaymentForm) {
        return null;
      }
      if (
        !this.initialPaymentForm.get(InitialPaymentFormFields.sameAsShipping)
          .value
      ) {
        return Validators.required(c);
      }
      return null;
    };
  }

  public ngAfterViewInit(): void {
    this.layoutService.enableTimeout();
    this.subs.push(
      this.initialPaymentForm.get('state').valueChanges.subscribe(x => {
        if (!x) {
          // Remove zip validation if its there. Since its tied to this control
          const zipControl = this.initialPaymentForm.get(
            InitialPaymentFormFields.zipOnCard
          );
          zipControl.setErrors({});
          return;
        }
      }),

      this.initialPaymentForm
        .get(InitialPaymentFormFields.zipOnCard)
        .valueChanges.subscribe(x => {
          if (!x || (x.length !== 5 && x.length !== 9)) {
            return;
          }
        })
    );

    this.subs.push(
      this.initialPaymentForm
        .get('sameAsShipping')
        .valueChanges.subscribe(x => {
          this.sameAsShipping = x;
          this.initialPaymentForm
            .get(InitialPaymentFormFields.streetAddress1)
            .updateValueAndValidity();
          this.initialPaymentForm
            .get(InitialPaymentFormFields.streetAddress1)
            .updateValueAndValidity();
          this.initialPaymentForm
            .get(InitialPaymentFormFields.cityOnCard)
            .updateValueAndValidity();
          this.initialPaymentForm
            .get(InitialPaymentFormFields.stateOnCard)
            .updateValueAndValidity();
          this.initialPaymentForm
            .get(InitialPaymentFormFields.zipOnCard)
            .updateValueAndValidity();
        })
    );
  }

  public ngOnDestroy(): void {
    this.subs.forEach(s => !s.closed && s.unsubscribe());
  }

  public onMaskChanged(isMasked: boolean): void {
    this.isMasked = isMasked;
  }

  public getZipStateMismatchError(baseError: string): string {
    const zipRep = /{{ +zip +}}/gi;
    const stateRep = /{{ +state +}}/gi;
    const newError = baseError
      .replace(
        stateRep,
        this.initialPaymentForm.get(InitialPaymentFormFields.stateOnCard).value
      )
      .replace(
        zipRep,
        this.initialPaymentForm.get(InitialPaymentFormFields.zipOnCard).value
      );
    return newError;
  }

  public onContinue(): void {
    this.customerState.updateCreditCard({
      customerId: this.customer.customerId,
      firstName: this.initialPaymentForm.get(
        InitialPaymentFormFields.firstNameOnCard
      ).value,
      lastName: this.initialPaymentForm.get(
        InitialPaymentFormFields.lastNameOnCard
      ).value,
      cardNumber: this.showFake ? '' : this.initialPaymentForm.get(InitialPaymentFormFields.cardNumber)
        .value,
      token: this.showFake ? this.savedToken : '',
      bin: this.showFake ? this.savedBin : '',
      expirationDate: this.initialPaymentForm.get(
        InitialPaymentFormFields.cardExpiration
      ).value,
      streetAddress1: this.sameAsShipping
        ? this.customer.address.address1
        : this.initialPaymentForm.get(InitialPaymentFormFields.streetAddress1).value,
      streetAddress2: this.sameAsShipping
        ? this.customer.address.address2
        : this.initialPaymentForm.get(InitialPaymentFormFields.streetAddress2).value,
      city: this.sameAsShipping ? this.customer.address.city : this.initialPaymentForm.get(InitialPaymentFormFields.cityOnCard).value,
      state: this.sameAsShipping ? this.customer.address.state : this.initialPaymentForm.get(InitialPaymentFormFields.stateOnCard).value,
      zip: this.sameAsShipping ? this.customer.address.zip : this.initialPaymentForm.get(InitialPaymentFormFields.zipOnCard).value
    } as CreditCardInformation);
    this.customerState.submitCreditCard().then(result => this.handleCardSubmitted(result));
  }

  public handleCardSubmitted(success: boolean): void {
    if (success) {
      if (!this.layoutService.editMode) {
        this.applyRouter.stepForward();
      } else {
        this.applyRouter.navigate(ApplyPageKey.review);
      }
    }
    else {
      this.initialPaymentForm.get(InitialPaymentFormFields.cardNumber).setErrors({ submitError: true });
      this.changeDetector.detectChanges();
    }
  }

  public disallowNonDigitInput(keyPressEvent: KeyboardEvent): boolean {
    const charCode = keyPressEvent.charCode || keyPressEvent.keyCode;
    const newChar = String.fromCharCode(charCode);
    const testRegex = /[^0-9]/g;
    if (testRegex.test(newChar)) {
      return false;
    }
  }

  public startEditCardNumber(): void {
    this.showFake = false;
    this.changeDetector.detectChanges();
    this.cardnumberElement.nativeElement.focus();
  }

  private getInitialForm(): FormGroup {
    return this.formBuilder.group(
      {
        [InitialPaymentFormFields.firstNameOnCard]: [
          '',
          [Validators.required, nameFormat, startsWithLetter],
        ],
        [InitialPaymentFormFields.lastNameOnCard]: [
          '',
          [Validators.required, nameFormat, startsWithLetter],
        ],
        [InitialPaymentFormFields.cardNumber]: [
          '',
          [this.creditCardConditionallyRequired(), creditCard],
        ],
        [InitialPaymentFormFields.fakeCard]: [],
        [InitialPaymentFormFields.cardExpiration]: [
          '',
          [Validators.required, expirationDate],
        ],
        [InitialPaymentFormFields.sameAsShipping]: [this.sameAsShipping],
        [InitialPaymentFormFields.streetAddress1]: [
          '',
          [this.conditionallyRequired(), street1Validator],
        ],
        [InitialPaymentFormFields.streetAddress2]: ['', [street1Validator]],
        [InitialPaymentFormFields.cityOnCard]: [
          '',
          [this.conditionallyRequired(), cityValidator],
        ],
        [InitialPaymentFormFields.stateOnCard]: [
          '',
          [this.conditionallyRequired(), stateValidator],
        ],
        [InitialPaymentFormFields.zipOnCard]: [
          '',
          [this.conditionallyRequired(), zipValidator],
        ]
      },
      {
        validator: validateStateZipMatch(
          this.zipCheckLoadingSubject,
          this.locationService
        ),
      }
    );
  }

  private updateCardInfo(
    customer: Customer,
    card: CreditCardInformation
  ): void {
    this.customer = customer;
    this.setFormValue(
      InitialPaymentFormFields.firstNameOnCard,
      card?.firstName || customer.firstName
    );
    this.setFormValue(
      InitialPaymentFormFields.lastNameOnCard,
      card?.lastName || customer.lastName
    );
    if (card && card.token?.length > 0) {
      this.setFormValue(InitialPaymentFormFields.fakeCard, `••••••••••••${card.token.substring(card.token.length - 4)}`);
      this.showFake = true;
      this.savedToken = card.token;
      this.savedBin = card.bin;
    }
    this.setFormValue(InitialPaymentFormFields.cardNumber, '');
    this.setFormValue(
      InitialPaymentFormFields.cardExpiration,
      card?.expirationDate
    );
    this.setFormValue(InitialPaymentFormFields.streetAddress1, card.streetAddress1);
    this.setFormValue(InitialPaymentFormFields.streetAddress2, card.streetAddress2);
    this.setFormValue(InitialPaymentFormFields.cityOnCard, card.city);
    this.setFormValue(InitialPaymentFormFields.stateOnCard, card.state);
    this.setFormValue(InitialPaymentFormFields.zipOnCard, card.zip);
  }

  private setFormValue(key: InitialPaymentFormFields, value: any) {
    if (!value) {
      return;
    }
    this.initialPaymentForm.get(key).setValue(value);
  }

  private isShippingAddressBillingAddress(): void {
    combineLatest([
      this.customerState.address$,
      this.customerState.card$,
    ]).pipe(
      filter(([address, card]) =>
        (card?.streetAddress1 && address?.address1 !== card?.streetAddress1) ||
        (card?.streetAddress2 && address?.address2 !== card?.streetAddress2) ||
        (card?.city && address?.city !== card?.city) ||
        (card?.state && address?.state !== card?.state) ||
        (card?.zip && address?.zip !== card?.zip)
      )
    ).subscribe(([address, card]) => {
      this.sameAsShipping = false;
    });
  }
}