
using System.Text.Json;

namespace origination.ui.HttpExceptionHandler
{
    public class HttpErrorDetail
    {
        public string Message { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}