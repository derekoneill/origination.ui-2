import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Pipe({
  name: 'inlineTranslate',
})
export class InlineTranslatePipe implements PipeTransform {
    constructor(
        private readonly translateService: TranslateService,
        private readonly http: HttpClient
    ) {}

  transform(key: string, languageCode: string): Observable<string> {
    const language = this.translateService.getLangs().includes(languageCode)
      ? languageCode
      : this.translateService.currentLang;
    return this.http.get(`./assets/translations/${language}.json`).pipe(
      map((content) => {
        let translation = content;
        key.split('.').forEach((k) => {
          translation = translation[k];
        });
        return translation as string;
      })
    );
  }
}