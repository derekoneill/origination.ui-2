using ProgLeasing.System.Logging.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace origination.ui.LogService
{
    public class LogRequest
    {
        public LogLevel LogLevel { get; set; }
        public string Message { get; set; }
        public Dictionary<string, string> Details { get; set; }
    }
}
