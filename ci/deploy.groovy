pipeline {
	agent { label 'Deployment_DEV' }

	options {
		//Only one concurrent build at a time on this project
        disableConcurrentBuilds()
        //Build discard policy
        buildDiscarder(logRotator(numToKeepStr: '5'))
	}

	parameters {
        choice(name: 'Environment', choices: [
            'QAENV08',
            'QAENV12',
            'QAENV13',
            'QAENV15',
            'QAENV19',
            'DEMO',
            'DEMO2',
            'RC',
            'PROD'
        ], description: 'environments to deploy to.')
        string(name: 'RepoName', defaultValue: 'origination.ui', description: 'Repo name to deploy.')
        string(name: 'BuildNumber', description: 'Upstream build number.')
    }

	stages {
		stage("Deploy") {
			steps {
				script {
                    checkout(changelog: false, scm: [$class: 'GitSCM', branches: [[name: '*/SSID']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'cm_automation']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'bitbucket_ssh', url: 'git@bitbucket.org:progfin-ondemand/cm_automation.git']]])

                    echo "parameters: ${params}"

                    echo "pipeline is about to get the SSID DB"

                    //Select Prod instance of SSID DB
                    pwsh(returnStdout: true, label: 'Set Prod Credentials for SSID DB', script: """
                        Import-Module -Name .\\cm_automation\\Common\\PLDeployment.psm1 -Force -DisableNameChecking
                        Copy-ApproveScripts -Environment 'PRD' -DestinationServer 'Jenkins' -ScriptLibraryPath '.\\cm_automation' -PSColors \$true
                        """)

                    //Get deploy information from the SSID DB
                    def SOFTWARE_NAME = pwsh(returnStdout: true, label: 'Get SoftwareName from SSID', script: """
                        Import-Module -Name .\\cm_automation\\Common\\DB.psm1 -Force -DisableNameChecking
                        \$Result = Get-SoftwareListing | Where-Object { \$_.URL -eq 'https://bitbucket.org/progfin-ondemand/${params.RepoName}' }
                        \$Result | Select-Object -ExpandProperty SoftwareName
                        """).trim()
                    echo"${SOFTWARE_NAME}"
                    def PROJECT_NAME = pwsh(returnStdout: true, label: 'Get RepositoryName from SSID', script: """
                        Import-Module -Name .\\cm_automation\\Common\\DB.psm1 -Force -DisableNameChecking
                        \$Result = Get-SoftwareListing | Where-Object { \$_.URL -eq 'https://bitbucket.org/progfin-ondemand/${params.RepoName}' }
                        \$Result | Select-Object -ExpandProperty RepositoryName
                        """).trim()
                    echo"${PROJECT_NAME}"
                    def SOFTWAREID = pwsh(returnStdout: true, label: 'Get SoftwareId from SSID', script: """
                        Import-Module -Name .\\cm_automation\\Common\\DB.psm1 -Force -DisableNameChecking
                        \$Result = Get-SoftwareListing | Where-Object { \$_.URL -eq 'https://bitbucket.org/progfin-ondemand/${params.RepoName}' }
                        \$Result | Select-Object -ExpandProperty SoftwareId
                        """).trim()
                    echo"${SOFTWAREID}"

                    build job: 'CICDDeployment/master', wait: true, parameters: [
                        [$class: 'StringParameterValue', name: 'Environment', value: "$params.Environment"],
                        [$class: 'StringParameterValue', name: 'SoftwareId', value: "$SOFTWAREID"],
                        [$class: 'StringParameterValue', name: 'BuildNumber', value: "$params.BuildNumber"],
                    ]
				}
			}
		}
	}
}
