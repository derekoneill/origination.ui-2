Function UiLint{
  $originalLocation = $PSScriptRoot
  $rootPath = ProjectRootPath
  $clientPath = Join-Path -Path $rootPath -ChildPath 'origination.ui/client-apps'
  Set-Location $clientPath
  npm run lint
  Set-Location $originalLocation
}

Function RunUiTests {  
  $rootPath = ProjectRootPath
  $originalLocation = $PSScriptRoot
  $clientPath = Join-Path -Path $rootPath -ChildPath 'origination.ui/client-apps'
  Set-Location $clientPath
  npm run test
  Set-Location $originalLocation
}

Function RunDotnetTests {  
  $rootPath = ProjectRootPath
  $originalLocation = $PSScriptRoot
  Set-Location $rootPath
  dotnet test
  Set-Location $originalLocation
}

Function ProjectRootPath {
  Join-Path -Path $PSScriptRoot -ChildPath '../../../'
}

Export-ModuleMember -Function UiLint
Export-ModuleMember -Function RunDotnetTests
Export-ModuleMember -Function RunUiTests