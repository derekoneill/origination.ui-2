import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import {
  ApplyPageKey,
  OnboardingState,
  ApplicationState,
  CustomerState
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { PrefillComponent } from './prefill.component';

describe('BankInfo Component', () => {
  let component: PrefillComponent;

  let mockFormBuilder: SubstituteOf<FormBuilder>;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockCustomerState: SubstituteOf<CustomerState>;

  beforeEach(() => {
    mockFormBuilder = Substitute.for<FormBuilder>();
    mockLayoutService = Substitute.for<LayoutService>();
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockOnboardingState = Substitute.for<OnboardingState>();
    mockApplicationState = Substitute.for<ApplicationState>();
    mockCustomerState = Substitute.for<CustomerState>();

    component = new PrefillComponent(
      mockFormBuilder,
      mockOnboardingState,
      mockApplicationState,
      mockCustomerState,
      mockLayoutService,
      mockApplyRouter,
    );
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});