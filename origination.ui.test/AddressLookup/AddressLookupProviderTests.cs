using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using origination.ui.AddressLookupService;

namespace origination.ui.test.AddressLookup
{
    public class AddressLookupProviderTests
    {
        private Mock<HttpMessageHandler> mockHttpMessageHandler;
        private HttpClient httpClient;
        private AddressLookupProvider provider;

        [SetUp]
        public void Setup()
        {
            mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            httpClient = new HttpClient(mockHttpMessageHandler.Object)
            {
                BaseAddress = new Uri("http://127.0.0.1")
            };

            provider = new AddressLookupProvider(httpClient);
        }

        [TestCase("REQUEST_DENIED")]
        [TestCase("ZERO_RESULTS")]
        [TestCase("0")]
        [TestCase("")]
        public async Task ReturnNullWhenApiStatusReturnedIsNotOk(string responseStatus)
        {
            var apiResponse = new GoogleGeocodeApiResponse
            {
                status = responseStatus
            };
            mockHttpMessageHandler.Protected().Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            ).ReturnsAsync(new HttpResponseMessage
            {
                Content = new StringContent(JsonSerializer.Serialize(apiResponse))
            }).Verifiable();

            var result = await provider.GetCityAndStateByZip("zip");
            Assert.IsNull(result);
        }

        [Test]
        public async Task ReturnNullWhenApiStatusReturnedIsNull()
        {
            var apiResponse = new GoogleGeocodeApiResponse
            {
                status = null
            };
            mockHttpMessageHandler.Protected().Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            ).ReturnsAsync(new HttpResponseMessage
            {
                Content = new StringContent(JsonSerializer.Serialize(apiResponse))
            }).Verifiable();

            var result = await provider.GetCityAndStateByZip("zip");
            Assert.IsNull(result);
        }

        [Test]
        public async Task ReturnCityWhenLocalityPresentInResponse()
        {
            var components = new List<GeocodeResult>();
            components.Add(new GeocodeResult()
            {
                address_components = new List<AddressComponent>()
                {
                    new AddressComponent()
                    {
                        short_name = "ShortCity",
                        long_name = "Long",
                        types = new List<string>()
                        {
                            "locality"
                        }
                    }
                }
            });
            var apiResponse = new GoogleGeocodeApiResponse
            {
                results = components,
                status = "OK"
            };
            mockHttpMessageHandler.Protected().Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            ).ReturnsAsync(new HttpResponseMessage
            {
                Content = new StringContent(JsonSerializer.Serialize(apiResponse))
            }).Verifiable();

            var result = await provider.GetCityAndStateByZip("zip");
            Assert.AreEqual("ShortCity", result.City);
        }

        [Test]
        public async Task ReturnStateWhenAdministrativeAreaLevel1PresentInResponse()
        {
            var components = new List<GeocodeResult>();
            components.Add(new GeocodeResult()
            {
                address_components = new List<AddressComponent>()
                {
                    new AddressComponent()
                    {
                        short_name = "ShortState",
                        long_name = "Long",
                        types = new List<string>()
                        {
                            "administrative_area_level_1"
                        }
                    }
                }
            });
            var apiResponse = new GoogleGeocodeApiResponse
            {
                results = components,
                status = "OK"
            };
            mockHttpMessageHandler.Protected().Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            ).ReturnsAsync(new HttpResponseMessage
            {
                Content = new StringContent(JsonSerializer.Serialize(apiResponse))
            }).Verifiable();

            var result = await provider.GetCityAndStateByZip("zip");
            Assert.AreEqual("ShortState", result.State);
        }
    }
}
