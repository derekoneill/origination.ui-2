import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { LayoutElements, LayoutService } from './layout.service';

describe('LayoutService', () => {
  let service: LayoutService;

  beforeEach(() => {
    service = new LayoutService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Update layout elements state', () => {
    let layoutElements: LayoutElements;
    let layoutSubject: Subject<LayoutElements>;

    beforeEach(() => {
      layoutElements = {
        hasFooter: chance.bool(),
        hasHeaderExitButton: chance.bool(),
        hasHeaderBackButton: chance.bool(),
        showTimeoutPrompt: chance.bool(),
      } as LayoutElements;
      layoutSubject = new Subject<LayoutElements>();
      layoutSubject.next(layoutElements);
    });

    it('should let the state know the footer should be visible', () => {
      let isFooterVisible: boolean;
      service.showFooter();
      service.layoutElements$.subscribe(layout => (isFooterVisible = layout.hasFooter));
      expect(isFooterVisible).toBe(true);
    });

    it('should let the state know the footer should not be visible', () => {
      let isFooterVisible: boolean;
      service.hideFooter();
      service.layoutElements$.subscribe(layout => (isFooterVisible = layout.hasFooter));
      expect(isFooterVisible).toBe(false);
    });

    it('should let the state know the back button should be visible', () => {
      let isBackButtonVisible: boolean;
      service.showHeaderBackButton();
      service.layoutElements$.subscribe(layout => (isBackButtonVisible = layout.hasHeaderBackButton));
      expect(isBackButtonVisible).toBe(true);
    });

    it('should let the state know the back button should not be visible', () => {
      let isBackButtonVisible: boolean;
      service.hideHeaderBackButton();
      service.layoutElements$.subscribe(layout => (isBackButtonVisible = layout.hasHeaderBackButton));
      expect(isBackButtonVisible).toBe(false);
    });

    it('should let the state know the exit button should be visible', () => {
      let isExitButtonVisible: boolean;
      service.showHeaderExitButton();
      service.layoutElements$.subscribe(layout => (isExitButtonVisible = layout.hasHeaderExitButton));
      expect(isExitButtonVisible).toBe(true);
    });

    it('should let the state know the exit button should not be visible', () => {
      let isExitButtonVisible: boolean;
      service.hideHeaderExitButton();
      service.layoutElements$.subscribe(layout => (isExitButtonVisible = layout.hasHeaderExitButton));
      expect(isExitButtonVisible).toBe(false);
    });

    it('should set the edit mode to true', () => {
      service.enableEditMode();
      expect(service.editMode).toBe(true);
    });

    it('should set the edit mode to false', () => {
      service.disableEditMode();
      expect(service.editMode).toBe(false);
    });
  });
});