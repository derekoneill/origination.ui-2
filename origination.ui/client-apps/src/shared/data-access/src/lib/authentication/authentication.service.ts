import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfig } from '../config';
import { AuthCustomer } from '../models';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient, private appConfig: AppConfig) { }

  getJwt(code: string): Observable<AuthCustomer> {
    return this.http.post<AuthCustomer>(
      `auth/codes/exchange`,
      { code }
    );
  }
}