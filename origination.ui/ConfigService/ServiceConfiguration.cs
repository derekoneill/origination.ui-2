using System.Collections.Generic;
using System.Linq;

namespace origination.ui.ConfigService
{
    public enum ServiceName
    {
        Auth,
        Content,
        Customer,
        Ecom,
        LeasePaymentEstimator,
        LeasePaymentTool,
        Origination,
        Store,
        Payments,
        Ui,
        Comprehension
    }

    public class ServiceConfiguration
    {
        public string CustomerApiUrl { get; set; }
        public string EcommerceApiUrl { get; set; }
        public string AuthApiUrl { get; set; }
        public string AuthClientUrl { get; set; }
        public string AddressLookupUrl { get; set; }
        public string CustomerPaymentOrchUrl { get; set; }
        public string StoreLocatorServiceUrl { get; set; }
        public string PaymentServiceApiUrl { get; set; }
        public string OriginationApiUrl { get; set; }
        public string ProgApplyClientUrl { get; set; }
        public string LeasePaymentEstimatorUrl { get; set; }
        public string LeasePaymentToolUrl { get; set; }
        public static Dictionary<ProgEnvironment, Dictionary<ServiceName, string>> environmentServiceUrls = new Dictionary<ProgEnvironment, Dictionary<ServiceName, string>> {
            {
                ProgEnvironment.LOCALHOST, new Dictionary<ServiceName, string> {
                    { ServiceName.Auth, "https://vdc-qaswebapp08.stormwind.local/Origination.Auth.Client" },
                    { ServiceName.Content, "https://vdc-qaswebapp13.stormwind.local/AppMeMicroConfig" },
                    { ServiceName.Customer, "https://vdc-qaswebapp13.stormwind.local/Origination.customer.api" },
                    { ServiceName.Ecom, "https://vdc-qaswebapp13.stormwind.local/ecomapi" },
                    { ServiceName.LeasePaymentEstimator, "https://vdc-qaswebapp08.stormwind.local/lease.paymentcalculator" },
                    { ServiceName.LeasePaymentTool, "https://www.progressivelp.com/PaymentToolUi/lease-payment-tool/lease-payment-tool.js" },
                    { ServiceName.Origination, "https://vdc-qaswebapp13.stormwind.local/originapi" },
                    { ServiceName.Store, "https://vdc-qaswebapp01.stormwind.local/StoreLocatorService" },
                    { ServiceName.Payments, "https://vdc-qaswebapp23.stormwind.local/CustomerPaymentOrch" },
                    { ServiceName.Ui, "https://vdc-qaswebapp13.stormwind.local/ProgApply" },
                    { ServiceName.Comprehension, "https://vdc-qaswebapp13.stormwind.local/CustomerComprehensionOrch" }
                }
            },
            {
                ProgEnvironment.QA, new Dictionary<ServiceName, string> {
                    { ServiceName.Auth, "https://vdc-qaswebapp08.stormwind.local/Origination.Auth.Client" },
                    { ServiceName.Content, "https://vdc-qaswebapp13.stormwind.local/AppMeMicroConfig" },
                    { ServiceName.Customer, "https://vdc-qaswebapp13.stormwind.local/Origination.customer.api" },
                    { ServiceName.Ecom, "https://vdc-qaswebapp13.stormwind.local/ecomapi" },
                    { ServiceName.LeasePaymentEstimator, "https://vdc-qaswebapp08.stormwind.local/lease.paymentcalculator" },
                    { ServiceName.LeasePaymentTool, "https://www.progressivelp.com/PaymentToolUi/lease-payment-tool/lease-payment-tool.js" },
                    { ServiceName.Origination, "https://vdc-qaswebapp13.stormwind.local/originapi" },
                    { ServiceName.Store, "https://vdc-qaswebapp01.stormwind.local/StoreLocatorService" },
                    { ServiceName.Payments, "https://vdc-qaswebapp23.stormwind.local/CustomerPaymentOrch" },
                    { ServiceName.Ui, "https://vdc-qaswebapp13.stormwind.local/ProgApply" },
                    { ServiceName.Comprehension, "https://vdc-qaswebapp13.stormwind.local/CustomerComprehensionOrch" }
                }
            }
        };

        public static Dictionary<ServiceName, string> GetProgApplyUrls(ProgEnvironment environment)
        {
            var requiredUiServices = new List<ServiceName>
            {
                ServiceName.Ecom,
                ServiceName.Auth,
                ServiceName.Ui,
                ServiceName.LeasePaymentEstimator,
                ServiceName.LeasePaymentTool,
            };
            return requiredUiServices.ToDictionary(x => x, x => ServiceConfiguration.GetBaseUrl(environment, x));
        }

        public static string GetBaseUrl(ProgEnvironment environment, ServiceName service)
        {
            // if the environment is qa get the number and replace the qa slug with the number
            return environmentServiceUrls[environment][service];
        }
    }
}