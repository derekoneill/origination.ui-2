import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ApplicationState } from './application/application-state';
import { AuthInterceptor } from './authentication/auth-interceptor';
import { AuthState } from './authentication/auth-state';
import {
  AddressApiService,
  AppConfig,
  ApplicationService,
  AuthenticationService,
  CustomerService,
  CustomerState,
  LocationService,
  StoreLocatorService
} from './index';
import { LeaseEstimatesState } from './lease/lease-estimates-state';
import { OnboardingState } from './onboarding/onboarding-state';


@NgModule({
  imports: [HttpClientModule],
  providers: [
    AddressApiService,
    AppConfig,
    ApplicationService,
    ApplicationState,
    AuthenticationService,
    AuthState,
    CustomerState,
    CustomerService,
    LeaseEstimatesState,
    LocationService,
    OnboardingState,
    StoreLocatorService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
})
export class DataAccessModule { }