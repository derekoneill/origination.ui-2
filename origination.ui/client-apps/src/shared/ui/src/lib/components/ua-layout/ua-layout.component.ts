import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'ua-layout',
  templateUrl: './ua-layout.component.html',
})
export class UaLayoutComponent {
  @Output() back: EventEmitter<void> = new EventEmitter<void>();
  @Output() closeApp: EventEmitter<void> = new EventEmitter<void>();
  @Output() logoClick: EventEmitter<void> = new EventEmitter<void>();
  @Output() changeLanguage: EventEmitter<string> = new EventEmitter<string>();
  @Output() interruptIdle: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() currentLocalization$: Observable<string>;

  onBack() {
    this.back.emit();
  }

  onClose() {
    this.closeApp.emit();
  }

  onLogoClick() {
    this.logoClick.emit();
  }

  onLanguageChange(desiredLanguage) {
    this.changeLanguage.emit(desiredLanguage);
  }

  onInterruptIdle(requestLogout: boolean) {
    this.interruptIdle.emit(requestLogout);
  }
}