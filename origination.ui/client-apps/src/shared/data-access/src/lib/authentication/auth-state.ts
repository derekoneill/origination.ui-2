import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApplicationState } from '../application';
import { CreditCardInformation, CustomerState } from '../customer';
import { OnboardingState } from '../onboarding/onboarding-state';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthState {
  private readonly stateSubject = new BehaviorSubject<Auth>({
    jwt: undefined,
  });

  public constructor(
    private readonly authService: AuthenticationService,
    private readonly applicationState: ApplicationState,
    private readonly customerState: CustomerState,
    private readonly onboardingState: OnboardingState
  ) { }

  public handleAuthCode(code: string, onboardingGuid: string): void {
    this.onboardingState.requestOnboardingInfo(onboardingGuid);
    combineLatest([this.authService.getJwt(code), this.onboardingState.storeId$])
      .subscribe(([authCustomer, storeId]) => {
        if (!authCustomer || !storeId) {
          return;
        }
        this.applicationState.updateApplication({ storeId, customerId: authCustomer.customerId });
        this.customerState.loadCustomerInfo(authCustomer.customerId);
        this.customerState.updateCreditCard({ customerId: authCustomer.customerId } as CreditCardInformation);
        this.stateSubject.next({
          ...this.getSnapshot(),
          jwt: 'currentlyUnused',
        });
      });
    this.onboardingState.requestOnboardingInfo(onboardingGuid);
    this.customerState.loadCustomerInfo(5519846);
  }

  public get jwt(): Observable<string> {
    return this.stateSubject.pipe(map(auth => auth.jwt));
  }

  private getSnapshot() {
    return this.stateSubject.value;
  }
}

interface Auth {
  jwt: string;
}