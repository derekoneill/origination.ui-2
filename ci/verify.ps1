$currentLeaf = Split-Path -Path (Get-Location) -Leaf
if ($currentLeaf -ne "client-apps") {
  Throw "Please run the scripted commands from the root UI location. Ran them from $currentLocation"
}

Invoke-Expression (Join-Path $PSScriptRoot "npm-install.ps1")

Write-Host "Starting lint"
$lintJob = Start-Job -ScriptBlock {npm run lint}
Write-Host "Starting test"
$testJob = Start-Job -ScriptBlock {npm run test}
Get-Job | Wait-Job