import {
  ActivatedRouteSnapshot,
  ParamMap,
  RouterStateSnapshot
} from '@angular/router';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { ApplyPageKey, OnboardingState } from '@ua/shared/data-access';
import { chance } from 'jest-chance';
import { ReplaySubject } from 'rxjs';
import { ApplyRouter } from './apply-router';
import { OnboardingGuard } from './onboarding-guard';

describe('Onboarding Guard', () => {
  let onboardingGuard: OnboardingGuard;

  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;

  beforeEach(() => {
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockOnboardingState = Substitute.for<OnboardingState>();

    onboardingGuard = new OnboardingGuard(
      mockApplyRouter,
      mockOnboardingState
    );
  });

  it('should create', () => {
    expect(onboardingGuard).toBeDefined();
  });

  describe('Can Activate', () => {
    let onboardingSubject: ReplaySubject<string>;
    let mockRouteSnapshot: SubstituteOf<ActivatedRouteSnapshot>;
    let mockRouteState: SubstituteOf<RouterStateSnapshot>;

    beforeEach(() => {
      mockRouteSnapshot = Substitute.for<ActivatedRouteSnapshot>();
      mockRouteState = Substitute.for<RouterStateSnapshot>();

      onboardingSubject = new ReplaySubject<string>();
      mockOnboardingState.onboardingGuid$.returns(
        onboardingSubject.asObservable()
      );
    });

    describe('given there is a guid in state', () => {
      beforeEach(() => {
        onboardingSubject.next(chance.string());
      });

      it('should allow page to load', () => {
        let actual: boolean;
        onboardingGuard
          .canActivate(mockRouteSnapshot, mockRouteState)
          .subscribe(canActivate => (actual = canActivate));

        expect(actual).toBe(true);
      });
    });

    describe('given there is not a guid in state', () => {
      beforeEach(() => {
        sessionStorage.clear();
        onboardingSubject.next(undefined);
      });

      describe('given there is a guid in the url', () => {
        let onboardingGuid: string;

        beforeEach(() => {

          onboardingGuid = chance.string();
          mockRouteSnapshot.queryParamMap.returns({
            get: paramName => onboardingGuid,
          } as ParamMap);
        });

        it('should allow page to load', () => {
          let actual: boolean;
          onboardingGuard
            .canActivate(mockRouteSnapshot, mockRouteState)
            .subscribe(canActivate => (actual = canActivate));

          expect(actual).toBe(true);
        });

        it('should use the url to get onboarding info', () => {
          onboardingGuard
            .canActivate(mockRouteSnapshot, mockRouteState)
            .subscribe();

          mockOnboardingState.received().requestOnboardingInfo(onboardingGuid);
        });
      });

      describe('given there is a guid in the url and a guid in session storage', () => {
        let onboardingGuid: string;
        let storedOnboardingGuid: string;

        beforeEach(() => {
          onboardingGuid = chance.string();
          storedOnboardingGuid = chance.string();
          mockRouteSnapshot.queryParamMap.returns({
            get: paramName => onboardingGuid,
          } as ParamMap);
          sessionStorage.setItem('onboardingGuid', storedOnboardingGuid);
        });

        it('should allow page to load', () => {
          let actual: boolean;
          onboardingGuard
            .canActivate(mockRouteSnapshot, mockRouteState)
            .subscribe(canActivate => (actual = canActivate));

          expect(actual).toBe(true);
        });

        it('should use the guid from the url - not session storage - to get onboarding info', () => {
          onboardingGuard
            .canActivate(mockRouteSnapshot, mockRouteState)
            .subscribe();

          mockOnboardingState.received().requestOnboardingInfo(onboardingGuid);
        });

        it('should set the session storage guid to the url guid', () => {
          onboardingGuard
            .canActivate(mockRouteSnapshot, mockRouteState)
            .subscribe();

          expect(sessionStorage.getItem('onboardingGuid')).toBe(onboardingGuid);
        });

      });

      describe('given there is not a guid in the url', () => {
        let mockQueryParams: SubstituteOf<ParamMap>;

        beforeEach(() => {
          mockQueryParams = Substitute.for<ParamMap>();
          mockQueryParams.get('onboardingGuid').returns(undefined);
          mockRouteSnapshot.queryParamMap.returns(mockQueryParams);
        });

        describe('given there is a guid is session storage', () => {
          let onboardingGuid: string;

          beforeEach(() => {
            onboardingGuid = chance.guid();
            sessionStorage.setItem('onboardingGuid', onboardingGuid);
          });

          it('should allow page to load', () => {
            let actual: boolean;
            onboardingGuard
              .canActivate(mockRouteSnapshot, mockRouteState)
              .subscribe(canActivate => (actual = canActivate));

            expect(actual).toBe(true);
          });

          it('should use session storage to get onboarding info', () => {
            onboardingGuard
              .canActivate(mockRouteSnapshot, mockRouteState)
              .subscribe();

            mockOnboardingState.received().requestOnboardingInfo(onboardingGuid);
          });
        });

        describe('given there is no way to get the store', () => {
          beforeEach(() => {
            mockQueryParams.get('id_code').returns(undefined);
          });

          it('should 404', () => {
            onboardingGuard
              .canActivate(mockRouteSnapshot, mockRouteState)
              .subscribe();

            mockApplyRouter.received().navigate(ApplyPageKey.notFound);
          });

          it('should not allow page to load', () => {
            let actual: boolean;
            onboardingGuard
              .canActivate(mockRouteSnapshot, mockRouteState)
              .subscribe(canActivate => (actual = canActivate));

            expect(actual).toBe(false);
          });
        });
      });
    });
  });
});