using System;
using System.IO.Compression;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using origination.ui.ConfigService;
using origination.ui.HttpExceptionHandler;
using origination.ui.RequestProxy;
using ProgLeasing.Platform.Auth;
using ProgLeasing.System.Logging;
using ProgLeasing.System.Logging.Serilog;
using origination.ui.AddressLookupService;

namespace origination.ui
{
    public class Startup
    {
        private const String CLIENT_APP_ROOT = "client-apps";
        private const String APP_NAME = "origination-ui";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCompression();

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest;
            });

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = $"{CLIENT_APP_ROOT}/dist/apps/{APP_NAME}";
            });

            // Register the Swagger services
            services.AddSwaggerDocument();

            // Setup Auth
            services.AddProgLeasingJwtBearerAuthentication(Configuration);
            services.AddProgLeasingAuthorization(Configuration);

            var serviceConfiguration = Configuration.GetSection("Services").Get<ServiceConfiguration>();
            services.AddAddressLookupProvider(serviceConfiguration);
            services.AddHttpContextAccessor();
            services.AddHealthChecks();
            services.AddControllers();
            services.AddProgLogger(Configuration, builder => { builder.RegisterSerilog(); });
            services.AddApplicationInsightsTelemetry();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseMiddleware<RequestProxyMiddleware>();

            app.UseRouting();

            // Auth
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCustomExceptionMiddlewareHandler();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            // Register the Swagger generator and the Swagger UI middlewares
            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
                endpoints.MapControllers();
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = $"{CLIENT_APP_ROOT}";
                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
