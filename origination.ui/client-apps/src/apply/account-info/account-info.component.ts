import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  ApplyPageKey,
  CustomerState,
  Customer,
  OnboardingState,
  ApplicationState,
  Application
} from '@ua/shared/data-access';

import {
  dateOfBirthTextMask,
  ssnTextMask,
  phoneTextMask,
  validateSsn,
  MAX_AGE,
  MIN_AGE,
  day,
  month,
  year,
  ageRange,
  nameFormat,
} from '@ua/shared/utilities';

import { LayoutService } from '@ua/shared/ui';
import { Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';

export enum AccountFormFields {
  firstName = 'firstName',
  lastName = 'lastName',
  phoneNumber = 'phone',
  email = 'email',
  dob = 'dob',
  ssn = 'ssn'
}

@Component({
  selector: 'ua-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.scss'],
})
export class AccountInfoComponent implements OnInit, OnDestroy {
  public pageRoute: ApplyPageKey = ApplyPageKey.termsAndConditions;
  public accountForm: FormGroup;
  public birthdateTextMask = dateOfBirthTextMask;
  public ssnTextMask = ssnTextMask;
  public phoneTextMask = phoneTextMask;
  public isMasked = true;
  private subs: Subscription[] = [];
  private languageCode = 'en';
  private storeId: number;


  constructor(
    private formBuilder: FormBuilder,
    private readonly layoutService: LayoutService,
    private readonly customerState: CustomerState,
    private readonly applyRouter: ApplyRouter,
    private readonly onboardingState: OnboardingState,
    private readonly applicationState: ApplicationState,
  ) { }

  public ngOnInit(): void {
    this.layoutService.enableTimeout();
    this.initForm();
    this.subs.push(this.onboardingState.phoneNumber$.subscribe(number => this.updatePhone(number)),
      this.onboardingState.languageCode$.subscribe(code => this.languageCode = code),
      this.onboardingState.storeId$.subscribe(storeId => this.storeId = storeId));
  }

  public ngOnDestroy(): void {
    this.subs.forEach(s => !s.closed && s.unsubscribe());
  }

  public onContinue(): void {
    if (this.accountForm.valid) {
      this.customerState.createCustomer({
        firstName: this.accountForm.get(AccountFormFields.firstName).value,
        lastName: this.accountForm.get(AccountFormFields.lastName).value,
        ssn: this.accountForm.get(AccountFormFields.ssn).value,
        cellPhone: this.accountForm.get(AccountFormFields.phoneNumber).value,
        emailAddress: this.accountForm.get(AccountFormFields.email).value
      } as Customer, this.languageCode);
      this.subs.push(
        this.customerState.customer$.subscribe(newCust => {
          if (!newCust.customerId) {
            return;
          }

          this.applicationState.updateApplication({ customerId: newCust.customerId, storeId: this.storeId } as Application);

          if (!this.layoutService.editMode) {
            this.applyRouter.stepForward();
          } else {
            this.applyRouter.navigate(ApplyPageKey.review);
          }
        }));
    }
  }

  public onSsnMaskChanged(isMasked: boolean): void {
    this.isMasked = isMasked;
  }

  private updatePhone(number: string): void {
    this.accountForm.get(AccountFormFields.phoneNumber).setValue(number);
  }

  private initForm(): void {
    this.accountForm = this.formBuilder.group({
      [AccountFormFields.email]: ['', [Validators.required, Validators.email]],
      [AccountFormFields.phoneNumber]: [''],
      [AccountFormFields.firstName]: ['', [Validators.required, nameFormat]],
      [AccountFormFields.lastName]: ['', [Validators.required, nameFormat]],
      [AccountFormFields.dob]: ['', [Validators.required, ageRange(MIN_AGE, MAX_AGE), month, day, year]],
      [AccountFormFields.ssn]: ['', [Validators.required, validateSsn]]
    });
  }
}