import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { Application, ApplicationService, Decision } from '.';
import { ApplicationState } from './application-state';
import { DecisionStatus } from './application.dto';

describe('Application State', () => {
  let state: ApplicationState;

  let mockApplicationService: SubstituteOf<ApplicationService>;

  beforeEach(() => {
    mockApplicationService = Substitute.for<ApplicationService>();
    state = new ApplicationState(mockApplicationService);
  });

  it('should create', () => {
    expect(state).toBeDefined();
  });

  describe('Update Application', () => {
    it('should not replace old values when not supplied', () => {
      let actual: Application;
      state.application$.subscribe(application => {
        actual = application;
      });

      state.updateApplication({
        storeId: chance.string(),
      } as Application);
      state.updateApplication({} as Application);

      expect(actual.storeId).toBeDefined();
    });

    it('should replace values', () => {
      const expectedStoreId = chance.string();
      let actual: Application;
      state.application$.subscribe(application => {
        actual = application;
      });

      state.updateApplication({
        storeId: chance.string(),
      } as Application);
      state.updateApplication({
        storeId: expectedStoreId,
      } as Application);

      expect(actual.storeId).toBe(expectedStoreId);
    });
  });

  describe('Submit Application', () => {
    let application: Application;

    beforeEach(() => {
      application = {} as Application;
      state.updateApplication(application);
    });

    it('should submit the current application', () => {
      let current: Application;
      state.application$.subscribe(app => (current = app));
      state.updateApplication(application);
      const expected = { ...current, ...application };

      state.submitApplication();

      mockApplicationService.received().submitApplication(expected);
    });

    it('should update the decision based on the results of the submit', () => {
      const resultSubject = new Subject<Decision>();
      const expected = {} as Decision;
      let actual: Decision;
      state.decision$.subscribe(decision => (actual = decision));

      mockApplicationService
        .submitApplication(Arg.any())
        .returns(resultSubject.asObservable());

      state.submitApplication();
      resultSubject.next(expected);

      expect(actual).toBe(expected);
    });

    it('should update the decision approval status based on the results of the submit', () => {
      const resultSubject = new Subject<Decision>();
      const expected = chance.pickone([
        DecisionStatus.approved,
        DecisionStatus.denied,
        DecisionStatus.error,
        DecisionStatus.inProcess,
        DecisionStatus.pending,
        DecisionStatus.preApproved,
      ]);
      const decision = {
        status: expected,
      } as Decision;
      let actual: DecisionStatus;
      state.decisionStatus$.subscribe(status => (actual = status));

      mockApplicationService
        .submitApplication(Arg.any())
        .returns(resultSubject.asObservable());

      state.submitApplication();
      resultSubject.next(decision);

      expect(actual).toBe(expected);
    });
  });
});