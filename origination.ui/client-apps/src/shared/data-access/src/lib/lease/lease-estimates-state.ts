import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class LeaseEstimatesState {
  private readonly stateSubject = new BehaviorSubject<LeaseCostEstimator>(
    {} as LeaseCostEstimator
  );

  public get leaseCostEstimator$(): Observable<LeaseCostEstimator> {
    return this.stateSubject.asObservable();
  }

  public get leaseCostEstimatorQuote$(): Observable<LeaseCostEstimatorQuote> {
    return this.leaseCostEstimator$.pipe(map(lce => lce?.quote));
  }

  public get leaseCostEstimatorPage$(): Observable<LeaseCostEstimatorPage> {
    return this.leaseCostEstimator$.pipe(map(lce => lce?.viewingResults));
  }

  public setLeaseQuote(state: LeaseCostEstimatorQuote) {
    this.stateSubject.next({ ...this.getSnapshot(), quote: state });
  }

  public setLeaseQuoteError(error: any) {
    this.stateSubject.next({
      ...this.getSnapshot(),
      error,
      quote: undefined,
    });
  }

  public setLeaseCostViewingPage(
    leaseCostEstimatorPage: LeaseCostEstimatorPage
  ) {
    this.stateSubject.next({
      ...this.getSnapshot(),
      viewingResults: leaseCostEstimatorPage,
    });
  }

  public destroyLeaseCostEstimator() {
    this.stateSubject.next(undefined);
  }

  private getSnapshot(): LeaseCostEstimator {
    return this.stateSubject.value;
  }
}

export enum LeaseCostEstimatorPage {
  tool = 'tool',
  results = 'results',
}

export interface LeaseCostEstimatorQuote {
  costOfRental: number;
  initialBalance: number;
  initialCashPrice: number;
  initialPayment: number;
  initialPaymentHigh: number;
  invoiceAmount: number;
  leaseTermId: number;
  numberOfPayments: number;
  numberOfRecurringPayments: number;
  paymentFrequency: string;
  paymentFrequencyId: number;
  periodicPayment: number;
  quoteRequest: LeaseCostEstimatorRequest;
  termMonthEstimates: MonthEstimate[];
  termMonths: number;
}

export interface MonthEstimate {
  costOfLease: number;
  cumulativeAmountPaid: number;
  paymentAmount: number;
  paymentMonth: number;
  paymentNumber: number;
}

export interface LeaseCostEstimatorRequest {
  paymentFrequencyId: number;
  retailCashPrice: number;
  state: string;
}

export interface LeaseCostEstimator {
  viewingResults: LeaseCostEstimatorPage;
  quote: LeaseCostEstimatorQuote;
  error: any;
}