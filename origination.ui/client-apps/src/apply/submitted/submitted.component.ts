import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import {
  Application,
  ApplicationState,
  ApplyPageKey,
  Decision
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';

@Component({
  selector: 'ua-submitted-application',
  templateUrl: './submitted.component.html',
  styleUrls: ['./submitted.component.scss'],
})
export class SubmittedComponent implements OnInit, OnDestroy, AfterViewInit {
  pageRoute = ApplyPageKey.submittedApplication;
  subs: Subscription[] = [];
  application: Application;

  constructor(
    private readonly applyRouter: ApplyRouter,
    private readonly applicationState: ApplicationState,
    private readonly layoutService: LayoutService,
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.applicationState.application$.subscribe(
        application => (this.application = application)
      )
    );

    this.layoutService.hideFooter();
    this.layoutService.hideHeaderBackButton();
    this.layoutService.showHeaderExitButton();
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }

  ngAfterViewInit() {
    this.applicationState.submitApplication();
    this.applicationState.decision$.subscribe(subResults =>
      this.handleSubmissionResults(subResults)
    );
  }

  handleSubmissionResults(appResult: Decision) {
    if (appResult.leaseId <= 0) {
      return;
    }
    this.applyRouter.stepForward();
  }
}