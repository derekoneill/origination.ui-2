using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using origination.ui.ConfigService;
using ProgLeasing.System.Logging.Contract;

namespace origination.ui.RequestProxy
{
    public class RequestProxyMiddleware
    {
        private static readonly HttpClient httpClient = new HttpClient();
        private readonly RequestDelegate nextMiddleware;
        private readonly IConfiguration serviceConfiguration;
        private readonly ILogger<RequestProxyMiddleware> logger;

        public RequestProxyMiddleware(RequestDelegate nextMiddleware, IConfiguration configuration, ILogger<RequestProxyMiddleware> logger)
        {
            this.nextMiddleware = nextMiddleware;
            this.logger = logger;
            this.serviceConfiguration = configuration;
        }

        public async Task Invoke(HttpContext context)
        {
            var targetUri = BuildTargetUri(context.Request);

            if (targetUri != null)
            {
                var targetRequestMessage = CreateTargetMessage(context, targetUri);

                Console.WriteLine(targetRequestMessage.RequestUri.ToString());
                using (var responseMessage = await httpClient.SendAsync(targetRequestMessage, HttpCompletionOption.ResponseHeadersRead, context.RequestAborted))
                {
                    context.Response.StatusCode = (int)responseMessage.StatusCode;
                    CopyFromTargetResponseHeaders(context, responseMessage);
                    await responseMessage.Content.CopyToAsync(context.Response.Body);
                }
                if (context.Response.StatusCode >= 400)
                    logger.Log(LogLevel.Error, "Request Proxy Middleware Exception", null, GenerateLogDetails(context));

                return;
            }

            await nextMiddleware(context);
        }

        private Dictionary<string, string> GenerateLogDetails(HttpContext context)
        {
            var logDetails = new Dictionary<string, string>();
            logDetails.Add("Host", context.Request.Host.ToString());
            logDetails.Add("Path", context.Request.Path);
            logDetails.Add("QueryParams", context.Request.QueryString.ToString());
            logDetails.Add("ResponseCode", context.Response.StatusCode.ToString());
            return logDetails;
        }

        private HttpRequestMessage CreateTargetMessage(HttpContext context, Uri targetUri)
        {
            var requestMessage = new HttpRequestMessage();
            CopyFromOriginalRequestContentAndHeaders(context, requestMessage);

            requestMessage.RequestUri = GetUriWithQueryString(context, targetUri);
            requestMessage.Headers.Host = targetUri.Host;
            requestMessage.Method = new HttpMethod(context.Request.Method);

            return requestMessage;
        }

        private Uri GetUriWithQueryString(HttpContext context, Uri uriSoFar)
        {
            var queryDictionary = context.Request.Query.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());
            var newUri = uriSoFar.ToString();
            foreach (var kvp in queryDictionary)
            {
                if (kvp.Value.Contains(','))
                {
                    var subVals = kvp.Value.Split(',');
                    foreach (var sv in subVals)
                        newUri = QueryHelpers.AddQueryString(newUri, kvp.Key, sv);
                }
                else
                    newUri = QueryHelpers.AddQueryString(newUri, kvp.Key, kvp.Value);
            }
            return new Uri(newUri);
        }

        private void CopyFromOriginalRequestContentAndHeaders(HttpContext context, HttpRequestMessage requestMessage)
        {
            var requestMethod = context.Request.Method;

            if (!HttpMethods.IsGet(requestMethod) &&
              !HttpMethods.IsHead(requestMethod) &&
              !HttpMethods.IsDelete(requestMethod) &&
              !HttpMethods.IsTrace(requestMethod))
            {
                var streamContent = new StreamContent(context.Request.Body);
                requestMessage.Content = streamContent;
            }

            foreach (var header in context.Request.Headers)
            {
                requestMessage.Content?.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray());
            }
        }

        private void CopyFromTargetResponseHeaders(HttpContext context, HttpResponseMessage responseMessage)
        {
            foreach (var header in responseMessage.Headers)
            {
                context.Response.Headers[header.Key] = header.Value.ToArray();
            }

            foreach (var header in responseMessage.Content.Headers)
            {
                context.Response.Headers[header.Key] = header.Value.ToArray();
            }
            context.Response.Headers.Remove("transfer-encoding");
        }

        private Uri BuildTargetUri(HttpRequest request)
        {
            var environment = this.serviceConfiguration.GetProgEnvironment();
            var knownApiMap = new Dictionary<string, string> {
          {"/auth", ServiceConfiguration.GetBaseUrl(environment, ServiceName.Auth)},
          {"/content", ServiceConfiguration.GetBaseUrl(environment, ServiceName.Content)},
          {"/customer", ServiceConfiguration.GetBaseUrl(environment, ServiceName.Customer)},
          {"/ecom", ServiceConfiguration.GetBaseUrl(environment, ServiceName.Ecom)},
          {"/origination", ServiceConfiguration.GetBaseUrl(environment, ServiceName.Origination)},
          {"/store", ServiceConfiguration.GetBaseUrl(environment, ServiceName.Store) },
          {"/payments", ServiceConfiguration.GetBaseUrl(environment, ServiceName.Payments) },
          {"/comprehension", ServiceConfiguration.GetBaseUrl(environment, ServiceName.Comprehension) },
      };

            var knownKey = knownApiMap.Keys.FirstOrDefault(x => request.Path.StartsWithSegments(x));
            if (knownKey != null)
            {
                request.Path.StartsWithSegments(knownKey, out var remainingPath);
                var newUri = new Uri(knownApiMap[knownKey] + remainingPath);
                return newUri;
            }

            return null;
        }
    }
}