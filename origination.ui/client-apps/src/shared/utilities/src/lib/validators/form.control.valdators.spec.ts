import { FormControl } from '@angular/forms';
import { Substitute } from '@fluffy-spoon/substitute';
import {
  creditCard,
  routingNumber,
  validateMonthlyGrossIncome,
} from '@ua/shared/utilities';

describe('Form Control Validators', () => {
  describe('Validate monthly income', () => {
    it('Income is required', () => {
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(null);
      const validator = validateMonthlyGrossIncome();
      const res = validator(mockValField);
      expect(res.required).toBe(true);
    });

    it('Income too low', () => {
      const testIncome = '0';
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(testIncome);
      const validator = validateMonthlyGrossIncome();
      const res = validator(mockValField);
      expect(res.minRequirement).toBe(true);
    });

    it('Income too high', () => {
      const testIncome = '199999';
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(testIncome);
      const validator = validateMonthlyGrossIncome();
      const res = validator(mockValField);
      expect(res.maxRequirement).toBe(true);
    });

    it('Valid income', () => {
      const testIncome = '500';
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(testIncome);
      const validator = validateMonthlyGrossIncome();
      const res = validator(mockValField);
      expect(res).toBeNull();
    });
  });

  describe('Bank Routing number validator', () => {
    it('Detecting Routing proper length', () => {
      const testRoutingNumber = '1';
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(testRoutingNumber);
      const res = routingNumber()(mockValField);
      expect(res.lengthRequirement).toBe(true);
    });

    it('Detecting invalid check digit', () => {
      const testRoutingNumber = '091000018';
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(testRoutingNumber);
      const res = routingNumber()(mockValField);
      expect(res.invalidRoutingNumber).toBe(true);
    });

    it('Detecting valid routing number', () => {
      const testRoutingNumber = '091000019';
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(testRoutingNumber);
      const res = routingNumber()(mockValField);
      expect(res).toBeNull();
    });
  });

  describe('Credit Card validator', () => {
    it('Detect invalid CC Format', () => {
      const testRoutingNumber = '1-12-345-123';
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(testRoutingNumber);
      const res = creditCard(mockValField);
      expect(res.creditCardFormatting).toBe(true);
    });

    it('Detect invalid CC Number', () => {
      const testRoutingNumber = '4012888777781881';
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(testRoutingNumber);
      const res = creditCard(mockValField);
      expect(res.creditCardInvalid).toBe(true);
    });

    it('Detect valid CC Number', () => {
      const testRoutingNumber = '4012888888881881';
      const mockValField = Substitute.for<FormControl>();
      mockValField.value.returns(testRoutingNumber);
      const res = creditCard(mockValField);
      expect(res).toBeNull();
    });
  });
});