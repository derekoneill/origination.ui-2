Import-Module "$PSScriptRoot/Setup-Aws.psm1" -force

Function GetProdInstanceId {
    $prodIp = "34.235.217.87"
    $prodInstance = GetInstanceByIp $prodIp
    $prodInstance.InstanceId
}

Function SwitchHost {    
	param(
		[parameter(mandatory = $true, position = 0)]
		[ValidateNotNullOrEmpty()]
		[string]$newHostInstanceId
    )
    $instance = GetInstanceByInstanceId $newHostInstanceId
    if (!$instance) {
        throw "$instance is invalid. Must have an existing instance to switch prod to"
    }
    $prodId = GetProdInstanceId
    Write-Host "Switching the active prod from $prodId to $newHostInstanceId"
    if ($newHostInstanceId -eq $prodId) {        
        throw "New Host is already the prod host"
    }

    $prodIp = "34.235.217.87"
    Unregister-EC2Address -PublicIp $prodIp
    
    Register-EC2Address -InstanceId $newHostInstanceId -PublicIp $prodIp
}

Export-ModuleMember -Function GetProdInstanceId
Export-ModuleMember -Function SwitchHost