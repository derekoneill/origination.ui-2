import { ClickEventType, FormElementEngagementEventType } from './analytics-event-emission-enums';

export interface AnalyticsEventProperties {
    eventLabel: string;
    eventValue?: number;
}

export interface ClickEventEmission {
    eventProperties: AnalyticsEventProperties;
    type: ClickEventType;
}

export interface ClickEventProperties {
    eventProperties?: AnalyticsEventProperties;
    eventType?: ClickEventType;
}

export interface FormElementEmission {
    eventProperties: AnalyticsEventProperties;
    type: FormElementEngagementEventType;
}

export interface FormElementEventProperties {
    eventProperties?: AnalyticsEventProperties;
    type?: FormElementEngagementEventType;
}

export interface PaymentEstimatorResultEmission {
    eventValue?: number;
    paymentEstLeasePeriod: number;
    paymentEstFrequency: string;
    payEstEstimatedTotal: number;
    payEstCostOfLease: number;
    payEstTotalCostOfLease: number;
    payEstRecurringPayment: number;
    payEstPayments: number;
}

export interface VirtualPageviewEmission {
    location: Location;
    title: string;
}

export interface ErrorEventEmission {
    errorType: string;
    errorMessage: string;
}