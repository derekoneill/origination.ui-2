import { Injectable } from '@angular/core';
import { AppConfig } from '@ua/shared/data-access';

@Injectable()
export class AppStartup {

  constructor(
    private readonly configService: AppConfig,
  ) { }

  public load(): Promise<void> {
    return Promise.all([
      this.configService.load(),
    ]).then(() => { });
  }
}