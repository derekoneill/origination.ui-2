namespace origination.ui.HttpExceptionHandler
{
    /// <summary>
    /// Informational responses (100–199),
    /// Successful responses (200–299),
    /// Redirects (300–399),
    /// Client errors (400–499),
    /// and Server errors (500–599).
    /// </summary>
    public class HttpServerException : HttpException
    {
        public HttpServerException(int statusCode, string message) : base(statusCode, message)
        {
        }
    }
}