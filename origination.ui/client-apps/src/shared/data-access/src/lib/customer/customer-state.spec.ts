import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { Address, BankInformation, CreditCardInformation, Customer, IncomeInformation } from '.';
import { CustomerState } from './customer-state';
import { CustomerEnvelope, CustomerService } from './customer.service';
import { of } from 'rxjs';
import { DatePipe } from '@angular/common';
describe('Customer State', () => {
  let state: CustomerState;

  let mockCustomerService: SubstituteOf<CustomerService>;
  let mockDatePipe: SubstituteOf<DatePipe>;

  beforeEach(() => {
    mockCustomerService = Substitute.for<CustomerService>();
    mockDatePipe = Substitute.for<DatePipe>();

    state = new CustomerState(mockCustomerService, mockDatePipe);
  });

  it('should create', () => {
    expect(state).toBeDefined();
  });

  describe('Update Customer', () => {
    it('should not replace old values when not supplied', () => {
      let actual: Customer;
      state.customer$.subscribe(customer => {
        actual = customer;
      });

      state.updateCustomer({
        firstName: chance.string(),
      } as Customer);
      state.updateCustomer({} as Customer);

      expect(actual.firstName).toBeDefined();
    });

    it('should not replace old objects when not supplied', () => {
      let actual: Customer;
      state.customer$.subscribe(customer => {
        actual = customer;
      });

      state.updateCustomer({
        firstName: chance.string(),
        bank: { accountNumber: chance.string() },
        incomeSource: { monthlyGrossIncome: chance.string() },
        address: { city: chance.string() },
      } as Customer);
      state.updateCustomer({} as Customer);
      state.updateCustomer({
        bank: {},
        incomeSource: {},
      } as Customer);

      expect(actual.firstName).toBeDefined();
      expect(actual.bank.accountNumber).toBeDefined();
      expect(actual.incomeSource.monthlyGrossIncome).toBeDefined();
      expect(actual.address.city).toBeDefined();
    });

    it('should replace values', () => {
      const expectedName = chance.string();
      let actual: Customer;
      state.customer$.subscribe(customer => {
        actual = customer;
      });

      state.updateCustomer({
        firstName: chance.string(),
      } as Customer);
      state.updateCustomer({ firstName: expectedName } as Customer);

      expect(actual.firstName).toBe(expectedName);
    });
  });

  describe('Get Customer Status', () => {
    let customerId: number;

    beforeEach(() => {
      customerId = chance.integer();
      state.updateCustomer({ customerId } as Customer);
    });

    it('should make the request for the current customer status', () => {
      state.customerStatus$.subscribe();

      mockCustomerService.received().getCustomerStatus(customerId);
    });
  });

  describe('Update Bank Info', () => {
    it('should not replace old values when not supplied', () => {
      let actual: BankInformation;
      state.bank$.subscribe(bank => {
        actual = bank;
      });

      state.updateBankInformation({
        routingNumber: chance.string(),
      } as BankInformation);
      state.updateBankInformation({} as BankInformation);

      expect(actual.routingNumber).toBeDefined();
    });

    it('should replace values', () => {
      const expectedRoutingNumber = chance.string();
      let actual: BankInformation;
      state.bank$.subscribe(bank => {
        actual = bank;
      });

      state.updateBankInformation({
        routingNumber: chance.string(),
      } as BankInformation);
      state.updateBankInformation({
        routingNumber: expectedRoutingNumber,
      } as BankInformation);

      expect(actual.routingNumber).toBe(expectedRoutingNumber);
    });
  });

  describe('Update Income Info', () => {
    it('should not replace old values when not supplied', () => {
      let actual: IncomeInformation;
      state.income$.subscribe(income => {
        actual = income;
      });

      state.updateIncome({
        monthlyGrossIncome: chance.string(),
      } as IncomeInformation);
      state.updateIncome({} as IncomeInformation);

      expect(actual.monthlyGrossIncome).toBeDefined();
    });

    it('should replace values', () => {
      const expectedIncome = chance.string();
      let actual: IncomeInformation;
      state.income$.subscribe(income => {
        actual = income;
      });

      state.updateIncome({
        monthlyGrossIncome: chance.string(),
      } as IncomeInformation);
      state.updateIncome({
        monthlyGrossIncome: expectedIncome,
      } as IncomeInformation);

      expect(actual.monthlyGrossIncome).toBe(expectedIncome);
    });
  });

  describe('Update Credit Card', () => {

    it('should save the credit card ', () => {
      const card = {
        token: chance.string(),
      } as CreditCardInformation;

      state.updateCreditCard(card);
      state.submitCreditCard();

      mockCustomerService.received().saveCreditCard(card);
    });

  });

  describe('Update Address', () => {
    it('should not replace old values when not supplied', () => {
      let actual: Address;
      state.address$.subscribe(address => {
        actual = address;
      });

      state.updateAddress({
        address1: chance.string(),
      } as Address);
      state.updateAddress({} as Address);

      expect(actual.address1).toBeDefined();
    });

    it('should replace values', () => {
      const expectedStreet1 = chance.string();
      let actual: Address;
      state.address$.subscribe(address => {
        actual = address;
      });

      state.updateAddress({
        address1: chance.string(),
      } as Address);
      state.updateAddress({
        address1: expectedStreet1,
      } as Address);

      expect(actual.address1).toBe(expectedStreet1);
    });
  });

  describe('Load customer data', () => {
    it('should get all available properties and update', () => {
      const customerId = chance.integer();

      const expectedName = chance.string();

      mockCustomerService.getCustomerData(Arg.any()).returns(of ({ value: { firstName: expectedName } } as CustomerEnvelope));
      let actual: Customer;

      state.customer$.subscribe(customer => {
        actual = customer;
      });

      state.updateCustomer({
        firstName: chance.string(),
      } as Customer);

      state.loadCustomerInfo(customerId);

      expect(actual.firstName).toBe(expectedName);

      mockCustomerService.received().getCustomerData(customerId);
    });

    it('should not update properties that are null', () => {
      const customerId = chance.integer();

      const expectedName = chance.string();

      mockCustomerService.getCustomerData(Arg.any()).returns(of({
        value: { firstName: chance.string(), lastName: null }
      } as CustomerEnvelope));
      let actual: Customer;

      state.customer$.subscribe(customer => {
        actual = customer;
      });

      state.updateCustomer({
        firstName: chance.string(),
        lastName: expectedName
      } as Customer);

      state.loadCustomerInfo(customerId);

      expect(actual.lastName).toBe(expectedName);

      mockCustomerService.received().getCustomerData(customerId);
    });
    //should update info we do get
    //Should not update info we don't get
  });
});