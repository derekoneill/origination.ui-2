import { Injectable } from '@angular/core';
import { NavigationEnd, PRIMARY_OUTLET, Router, RouterEvent, UrlSegment, UrlSegmentGroup, UrlTree } from '@angular/router';
import { ApplicationState, Customer, CustomerState, Decision } from '@ua/shared/data-access';
import { combineLatest } from 'rxjs';
import { debounceTime, filter, startWith } from 'rxjs/operators';
import { Onboarding, OnboardingState } from '../data-access/src/lib/onboarding/onboarding-state';
import {
  ClickEventEmission,
  ErrorEventEmission,
  FormElementEmission
} from './analytics-event-emission-interfaces';
import { AnalyticsWindow } from './analytics-window';

@Injectable({
  providedIn: 'root',
})
export class AnalyticsService {
  readonly analyticsWindow: AnalyticsWindow = window as any;
  private applicationStartTime: Date;

  constructor(
    private readonly onboardingState: OnboardingState,
    private readonly customerState: CustomerState,
    private readonly applicationState: ApplicationState,
    private readonly router: Router,
  ) {
    this.pageViewEvents();
  }

  public trackUniversalButtonClick(clickEvent: ClickEventEmission): void {
    this.analyticsWindow.dataLayer.push({
      event: 'eventTracker',
      eventCategory: 'Application Engagement',
      eventAction: 'Button Click',
      eventLabel: clickEvent.eventProperties.eventLabel,
      eventValue: clickEvent.eventProperties.eventValue,
      eventNonInteraction: 0,
      hitType: 'event',
    });
  }

  public trackUniversalLinkClick(clickEvent: ClickEventEmission): void {
    this.analyticsWindow.dataLayer.push({
      event: 'eventTracker',
      eventCategory: 'Application Engagement',
      eventAction: 'Link Click',
      eventLabel: clickEvent.eventProperties.eventLabel,
      eventValue: clickEvent.eventProperties.eventValue,
      eventNonInteraction: 0,
      hitType: 'event',
    });
  }

  public trackFormElementEngagement(
    formElementEvent: FormElementEmission
  ): void {
    this.analyticsWindow.dataLayer.push({
      event: 'eventTracker',
      eventCategory: 'Application Engagement',
      eventAction: 'Form Element',
      eventLabel: formElementEvent.eventProperties.eventLabel,
      eventValue: formElementEvent.eventProperties.eventValue,
      eventNonInteraction: 0,
      hitType: 'event',
    });
  }

  public trackFormFill(formElementEvent: FormElementEmission): void {
    this.analyticsWindow.dataLayer.push({
      event: 'eventTracker',
      eventCategory: 'Application Engagement',
      eventAction: 'Form Fill',
      eventLabel: formElementEvent.eventProperties.eventLabel,
      eventValue: formElementEvent.eventProperties.eventValue,
      eventNonInteraction: 0,
      formFieldName: formElementEvent.eventProperties.eventLabel,
      hitType: 'event',
    });
  }

  public trackError(errorEvent: ErrorEventEmission): void {
    this.analyticsWindow.dataLayer.push({
      event: 'eventTracker',
      eventCategory: 'Errors',
      eventAction: errorEvent.errorType,
      eventLabel: errorEvent.errorMessage,
      eventValue: 0,
      eventNonInteraction: 0,
      errors: 1,
    });
  }

  public trackApplicationStart(isResuming: boolean = false, isReApplying: boolean = false): void {
    this.applicationStartTime = new Date();
    this.analyticsWindow.dataLayer.push({
      event: 'eventTracker',
      eventCategory: 'Lease Application',
      eventAction: 'Start Application',
      eventLabel: 'Success',
      applicationStarted: 1,
      applicationReapply: isReApplying ? 1 : 0,
      applicationsStarted: 1,
      applicationsResumed: isResuming ? 1 : 0,
      hitType: 'event'
    });
  }

  public trackApplicationSubmit(approvalStatus: string, leaseId: number, approvalAmount: number): void {
    this.analyticsWindow.dataLayer.push({
      event: 'eventTracker',
      eventCategory: 'Lease Application',
      eventAction: 'Submit Application',
      eventLabel: 'Success',
      approvalStatus,
      applicationSubmitted: 1,
      applicationsSubmitted: 1,
      applicationCompletionTime: Math.ceil((new Date().getTime() - this.applicationStartTime.getTime()) / 1000),
      leaseId,
      approvalAmount,
      hitType: 'event'
    });
  }

  public trackLocalizationInteraction(eventLabel: string, languageCode: string): void {
    this.analyticsWindow.dataLayer.push({
      event: 'eventTracker',
      eventCategory: 'Localization',
      eventAction: 'Localization Button Click',
      eventLabel,
      eventValue: 0,
      eventNonInteraction: 0,
      localizationUsed: 1,
      localizationLanguage: languageCode,
      hitType: 'event'
    });
  }

  public sessionTimeoutEvent() {
    this.analyticsWindow.dataLayer.push({
      event: Event.eventTracker,
      eventCategory: EventCategory.errors,
      eventAction: 'Session Timeout',
      eventLabel: '',
      eventValue: 0,
      eventNonInteraction: 1,
      hitType: HitType.eventTrackingHit,
    });
  }

  public pageViewEvents(): void {
    combineLatest([
      this.router.events.pipe(startWith(undefined as RouterEvent)),
      this.onboardingState.onboardingInfo$.pipe(startWith(undefined as Onboarding)),
      this.customerState.customer$.pipe(startWith(undefined as Customer)),
      this.applicationState.decision$.pipe(startWith(undefined as Decision))
    ])
      .pipe(
        filter(([routerEvent, onboardingInfo]) => routerEvent instanceof NavigationEnd && !!onboardingInfo?.onboardingGuid),
        debounceTime(0)
      )
      .subscribe(([routerEvent, onboardingInfo, customer, decision]) => {
        const location = this.router.url;
        this.analyticsWindow.dataLayer.push({
          title: this.getTitleFromUrl(location),
          page: this.getPageFromUrl(location),
          location: this.analyticsWindow.location.href,
          storeId: onboardingInfo.storeId,
          flowType: 'Unified App - Reduced',
          flowName: 'Reduced',
          hitType: HitType.pageTrackingHit,
          lenderName: 'Progressive',
          lenderType: 'First',
          textToApplyEnabled: 0,
          inStoreFlag: 1,
          paymentEstEnabled: 1,
          bankLookupEnabled: 0,
          rldEnabled: 0,
          localizationEnabled: 1,
          storeLocatorEnabled: 0,
          onboardingGuid: onboardingInfo.onboardingGuid,
          customerId: customer?.customerId ? customer.customerId : undefined,
          applicationId: decision?.leaseId ? decision.leaseId : undefined,
        });
      });
  }

  private getPageFromUrl(url: string): string {
    const tree: UrlTree = this.router.parseUrl(url);
    const segmentGroup: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];
    const segments: UrlSegment[] = segmentGroup.segments;
    const page = `/${segments.map(segment => segment.path).join('/')}`;

    return page;
  }

  private getTitleFromUrl(url: string): string {
    const tree: UrlTree = this.router.parseUrl(url);
    const segmentGroup: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];
    const segments: UrlSegment[] = segmentGroup.segments;
    const title = segments[segments.length - 1].path;

    return title;
  }
}

export enum HitType {
  pageTrackingHit = 'pageview',
  eventTrackingHit = 'event',
}

export enum Event {
  eventTracker = 'eventTracker',
  virtualPageview = 'virtualPageview',
}

export enum EventCategory {
  applicationEngagement = 'Application Engagement',
  applicationStart = 'Application Start',
  applicationResume = 'Application Resume',
  applicationSubmission = 'Application Submission',
  applicationExit = 'Application Exit',
  paymentEstimator = 'Payment Estimator',
  valuePropScreen = 'Value Prop Screen',
  otp = 'OTP',
  checkout = 'Checkout',
  navigation = 'Navigation',
  alert = 'Alert',
  errors = 'Errors',
}