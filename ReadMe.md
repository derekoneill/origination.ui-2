# [Project Name Here]
The [Project Name Here] is a ...

## Contact Information
For questions about the contents of this repository, please contact the [Group Name] or [Team Name] team at: [teamname]@progleasing.com

### Confluence:
- [Team Name](https://progfin.atlassian.net/wiki/spaces/[TBD])
- [C4 Diagrams](https://progfin.atlassian.net/wiki/spaces/[TBD])

### Slack:
- [Group Name](https://prog-technology.slack.com/archives/[TBD])
- [Team Name](https://prog-technology.slack.com/archives/[TBD])

### Jira:
- [Team Project](https://progfin.atlassian.net/browse/[TBD])

### Pipeline:
- [Jenkins](https://bbhook.progleasing.com/job/[Project])

### SonarQube:
- [Overview](http://sonarqube.stormwind.local:9000/dashboard?id=[Project])

### Logging:
- [Splunk](https://splunk.stormwind.local:8000/en-US/app/search/search?q=search%20[TBD]&display.page.search.mode=smart&dispatch.sample_ratio=1&earliest=-7d&latest=now)
- [AppInsights Demo](https://portal.azure.com/#@progleasing.onmicrosoft.com/resource/subscriptions/[TBD])
- [AppInsights Dev](https://portal.azure.com/#@progleasing.onmicrosoft.com/resource/subscriptions/[TBD])
- [AppInsights Prod](https://portal.azure.com/#@progleasing.onmicrosoft.com/resource/subscriptions/[TBD])
- [AppInsights QA](https://portal.azure.com/#@progleasing.onmicrosoft.com/resource/subscriptions/[TBD])
- [AppInsights RC](https://portal.azure.com/#@progleasing.onmicrosoft.com/resource/subscriptions/[TBD])

### Swagger Hosts: 
- [QAS](https://VDC-QASWEBAPP03.stormwind.local/[TBD]/swagger) 
- [QINT](https://VDC-QASWEBAPP08.stormwind.local/[TBD]/swagger)
- [Developer Portal](https://www.readme.com/[TBD])