import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplyPageKey } from '@ua/shared/data-access';
import { ApplyLayoutComponent } from './apply-layout/apply-layout.component';
import { BankInfoComponent } from './bank-info/bank-info.component';
import { ContactInfoComponent } from './contact-info/contact-info.component';
import { IncomeInfoComponent } from './income-info/income-info.component';
import { InitialPaymentComponent } from './initial-payment/initial-payment.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LeaseCostEstimatorManagerComponent } from './lease-cost-estimator-manager/lease-cost-estimator-manager.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { OnboardingGuard } from './onboarding-guard';
import { PreSubmitAppTermsSheetManagerComponent } from './pre-submit-app-terms-sheet-manager/pre-submit-app-terms-sheet-manager.component';
import { ResultsComponent } from './results/results.component';
import { ReviewComponent } from './review/review.component';
import { SubmittedComponent } from './submitted/submitted.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { AccountInfoComponent } from './account-info/account-info.component';
import { PrefillComponent } from './prefill/prefill.component';

const routes: Routes = [
  {
    path: '',
    component: ApplyLayoutComponent,
    runGuardsAndResolvers: 'always',
    children: [
      {
        path: ApplyPageKey.notFound,
        component: NotFoundComponent,
      },
      {
        path: ApplyPageKey.landingPage,
        component: LandingPageComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: ApplyPageKey.leaseCostEstimator,
        component: LeaseCostEstimatorManagerComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: ApplyPageKey.review,
        component: ReviewComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: ApplyPageKey.accountInfo,
        component: AccountInfoComponent,
        //canActivate: [OnboardingGuard]
      },
      {
        path: ApplyPageKey.termsAndConditions,
        component: TermsAndConditionsComponent,
        canActivate: [OnboardingGuard]
      },
      {
        path: ApplyPageKey.prefill,
        component: PrefillComponent,
        canActivate: [OnboardingGuard]
      },
      {
        path: ApplyPageKey.contactInfo,
        component: ContactInfoComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: ApplyPageKey.incomeInfo,
        component: IncomeInfoComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: ApplyPageKey.bankInfo,
        component: BankInfoComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: ApplyPageKey.submitApplicationTerms,
        component: PreSubmitAppTermsSheetManagerComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: ApplyPageKey.submittedApplication,
        component: SubmittedComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: ApplyPageKey.results,
        component: ResultsComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: ApplyPageKey.initialPayment,
        component: InitialPaymentComponent,
        canActivate: [OnboardingGuard],
      },
      {
        path: '**',
        redirectTo: ApplyPageKey.landingPage,
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplyRoutingModule {
  static components = [
    ApplyLayoutComponent,
    ContactInfoComponent,
    BankInfoComponent,
    IncomeInfoComponent,
    SubmittedComponent,
    InitialPaymentComponent,
    AccountInfoComponent
  ];
}