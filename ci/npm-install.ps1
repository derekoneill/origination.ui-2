Import-Module (Join-Path -Path $PSScriptRoot "file-utils.psm1")

$userPath = $HOME
$comparePackageJsonPath = Join-Path -Path "$userPath/progApply/build" "comparePackage.json"

if ((Test-Path -Path $comparePackageJsonPath) -and ((Get-FileHash -Path $comparePackageJsonPath).hash -eq (Get-FileHash -Path "package.json").hash)) {
  Write-Host "Npm install not required, moving on."  
}
else {
  Write-Host "Npm install required"
  Invoke-Expression "npm i --prune"
  New-Item -ItemType File -Path $comparePackageJsonPath -Force
  Copy-Item -Path "package.json" -Destination $comparePackageJsonPath -Force
}