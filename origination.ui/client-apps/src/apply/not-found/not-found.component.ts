import { Component, OnInit } from '@angular/core';
import { LayoutService } from '@ua/shared/ui';

@Component({
  selector: 'ua-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent implements OnInit {
  constructor(private readonly layoutService: LayoutService) { }

  public ngOnInit(): void {
    this.layoutService.hideHeaderBackButton();
    this.layoutService.hideHeaderExitButton();
    this.layoutService.hideFooter();
  }
}