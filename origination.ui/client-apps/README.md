# ClientApps

This project is the ProgApply UI.

## Development server

Run `ng serve` for a dev server.

## Code scaffolding

Run `ng g component my-component --project=my-app` to generate a new component.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests
Run a global jest (npm run test | npm run watch) and filter using jest features

[Jest](https://jestjs.io).