/* eslint-disable no-shadow */
import { AbstractControl, ValidatorFn } from '@angular/forms';

export const MIN_MONTHLY_INCOME = 1;
export const MAX_MONTHLY_INCOME = 99999.98;
export const MIN_AGE = 18;
export const MAX_AGE = 99;

const dobRegex = /^(0[1-9]|1[012]|[0-9])\/(0[1-9]|[12][0-9]|3[01]|[0-9])\/(\d{4})$/;

export const phone = (control: AbstractControl) => {
  const val: string = control.value;

  if (val[0] === '0' || val[0] === '1') {
    return { areaCode: true };
  }

  if (val.replace(/\D/gi, '').length !== 10) {
    return { formatting: true };
  }
  return null;
};

export const validateSsn = (control: AbstractControl) => {
  const val: string = control.value;
  const testRegex = /^(?!219099999|078051120)(?!666|000)\d{3}\d{2}\d{4}$/;
  if (val) {
    const testSSN = val.replace(/\D/g, '');
    if (testSSN.length !== 9) {
      return { formatting: true };
    }
    if (!testRegex.test(testSSN)) {
      return { formatting: true };
    } else {
    }
  }

  return null;
};

export const validateMonthlyGrossIncome = (): ValidatorFn => (
  ctrl: AbstractControl
): { [key: string]: any } => {
  if (ctrl) {
    let value = ctrl.value;
    if (!value) {
      return {
        required: true,
      };
    }

    value = parseFloat(value.replace(/([^0-9\.])/g, ''));
    if (value < MIN_MONTHLY_INCOME) {
      return {
        minRequirement: true,
      };
    }

    if (value > MAX_MONTHLY_INCOME) {
      return {
        maxRequirement: true,
      };
    }
  }
  return null;
};

export const startsWithLetter = (control: AbstractControl) => {
  const val: string = control.value;
  const testRegex = /^[a-zA-Z]/;
  if (val?.length > 0 && !testRegex.test(val)) {
    return { startsWithLetter: true };
  }
  return null;
};

export const nameFormat = (control: AbstractControl) => {
  const val: string = control.value;
  const testRegex = /[^a-zA-Z\s-']/g;
  if (testRegex.test(val)) {
    return { nameFormat: true };
  }
  return null;
};

/*
 * This was taken from https://github.com/DrShaffopolis/bank-routing-number-validator/blob/master/index.js
 */
export const isValidRoutingNumber = (routingNumber: string) => {
  if (!routingNumber) {
    // all 0's is technically a valid routing number, but it's inactive
    return false;
  }

  let routing = routingNumber.toString();
  while (routing.length < 9) {
    routing = '0' + routing;
  }

  // Has to be 9 digits
  const match = routing.match('^\\d{9}$');
  if (!match) {
    return false;
  }

  // The first two digits of the nine digit RTN must be in the ranges 00 through 12, 21 through 32, 61 through 72, or 80.
  // https://en.wikipedia.org/wiki/Routing_transit_number
  const firstTwo = parseInt(routing.substring(0, 2), 10);
  const firstTwoValid =
    (0 <= firstTwo && firstTwo <= 12) ||
    (21 <= firstTwo && firstTwo <= 32) ||
    (61 <= firstTwo && firstTwo <= 72) ||
    firstTwo === 80;
  if (!firstTwoValid) {
    return false;
  }

  // This is the checksum
  // http://www.siccolo.com/Articles/SQLScripts/how-to-create-sql-to-calculate-routing-check-digit.html
  const weights = [3, 7, 1];
  let sum = 0;
  for (let i = 0; i < 8; i++) {
    sum += parseInt(routing[i], 10) * weights[i % 3];
  }

  return (10 - (sum % 10)) % 10 === parseInt(routing[8], 10);
};

export const routingNumber = () => (control: AbstractControl) => {
  if (control && control.value) {
    const input = control.value;
    // Testing Numbers
    if (
      input === '000000001' ||
      input === '000000002' ||
      input === '000000003'
    ) {
      return null; // Allow Testing numbers through
    }

    if (!/^[0-9]{9}$/.test(input)) {
      return { lengthRequirement: true };
    } else if (!isValidRoutingNumber(input)) {
      return { invalidRoutingNumber: true };
    }
  }
  return null;
};

export const creditCard = (control: AbstractControl) => {
  if (control && control.value) {
    if (!doesCardMeetFormattingRequirement(control.value)) {
      return { creditCardFormatting: true };
    } else if (!luhnAlgorithmCheck(control.value)) {
      return { creditCardInvalid: true };
    }
  }
  return null;
};

export const expirationDate = (control: AbstractControl) => {
  if (control && control.value) {
    let value = control.value;
    value = value.replace(/[^0-9]/gi, '');
    const monthPart = value.substr(0, 2);
    const yearPart = value.substr(2, 2);
    const monthNumber = parseInt(monthPart, 10);
    if (monthNumber > 12 || monthNumber < 1) {
      return { invalidExpirationDate: true };
    }

    // today's date is in the year 2019, make sure in 81 years this is updated to pre-pend 21
    // you have been warned.
    const yearNumber = parseInt('20' + yearPart, 10);
    const today = new Date();
    if (yearNumber < today.getFullYear()) {
      return { invalidExpirationDate: true };
    } else if (
      yearNumber === today.getFullYear() &&
      monthNumber <= today.getMonth() + 1
    ) {
      return { invalidExpirationDate: true };
    }
  }

  return null;
};

export const doesCardMeetFormattingRequirement = (value: string) => {
  if (!value || !value.length) {
    return false;
  }
  value = value.replace(/[^0-9]/gi, '');
  if (value && value.length > 14) {
    return true;
  }
  return false;
};

export const luhnAlgorithmCheck = (cardNumber: string) => {
  // Clean the credit card input
  const cardNumberArray = cardNumber.replace(/\D/gi, '').split('');

  // The result of the algorithm
  let digitSum = 0;

  // Toggle for doubling every other digit
  let everyOtherToggle = false;

  // from right to left
  for (let i = cardNumberArray.length - 1; i >= 0; i--) {
    let numberDigit = parseInt(cardNumberArray[i], 10);

    if (everyOtherToggle) {
      // Every other digit is doubled
      numberDigit *= 2;

      // if the result of doubling greater than 9
      // use the sum of both digits (in this case we can subtract 9)
      if (numberDigit > 9) {
        numberDigit -= 9;
      }
    }

    // Add the result to the total algorithm sum;
    digitSum += numberDigit;

    // Flip the toggle
    everyOtherToggle = !everyOtherToggle;
  }

  // If digits sum is greater than 0
  // and divisible by 10, we are good.
  return digitSum > 0 && digitSum % 10 === 0;
};

export const month = (control: AbstractControl) => {
  if (control && control.value) {
    const value = control.value;
    const parsedMonth = parseInt(value.split('/')[0], 10);
    if (isNaN(parsedMonth) || parsedMonth > 12 || parsedMonth < 1) {
      return { month: true };
    }
  }
  return null;
};

export const day = (control: AbstractControl) => {
  if (control && control.value) {
    const value = control.value;
    const dateElements = value.split('/');
    const parsedMonth = parseInt(dateElements[0], 10);
    const parsedDay = parseInt(dateElements[1], 10);
    const year = parseInt(dateElements[2], 10);
    if (isNaN(parsedDay) && !isNaN(parsedMonth)) {
      return { day: true };
    } else if (isNaN(parsedDay)) {
      return null;
    } else if (parsedDay === 0 || parsedDay > 31) {
      return { day: true };
    }
    const daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (
      !isNaN(year) &&
      (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0))
    ) {
      // Account for leap years
      daysInMonth[1] = 29;
    }
    if (!isNaN(year) && parsedDay > daysInMonth[parsedMonth - 1]) {
      return { day: true };
    }
    return null;
  }
  return null;
};

export const year = (control: AbstractControl) => {
  if (control && control.value) {
    const value = control.value;
    const dateElements = value.split('/');
    const parsedMonth = parseInt(dateElements[0], 10);
    const parsedDay = parseInt(dateElements[1], 10);
    const parsedYear = parseInt(dateElements[2], 10);
    if (!isNaN(parsedMonth) && !isNaN(parsedDay) && isNaN(parsedYear)) {
      return { year: true };
    } else if (!isNaN(parsedYear) && (parsedYear < 1900 || parsedYear > 3000)) {
      return { year: true };
    }
  }
  return null;
};

export const ageRange = (minAge: number, maxAge: number) =>
   (control: AbstractControl) => {
    if (control && control.value) {
      const value = control.value.replace(/\s/g, '');
      if (dobRegex.test(value)) {
        const calcAge = calculateAge(value);
        if (calcAge < minAge) {
          return { minAge: true };
        } else if (calcAge > maxAge) {
          return { maxAge: true };
        }
      }
    }
    return null;
};

export const calculateAge = (dateString: string) => {
  const birth = new Date(dateString);
  const now = new Date();

  let age = now.getFullYear() - birth.getFullYear();

  // If now is JAN and birth is FEB we are still a year younger
  if (now.getMonth() < birth.getMonth()) {
    age -= 1;
  } else if (
    // else if Now is JAN 1 and BIRTH is JAN 2
    // We are still a year younger.
    now.getMonth() === birth.getMonth() &&
    now.getDate() < birth.getDate()
  ) {
    age -= 1;
  }
  return age;
};