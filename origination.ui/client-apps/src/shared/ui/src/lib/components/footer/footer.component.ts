import { DOCUMENT } from '@angular/common';
import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  Renderer2,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AnalyticsService } from '@ua/shared/analytics';
import { Observable } from 'rxjs';
import { LayoutElements, LayoutService } from '../../layout.service';

export enum LanguageCode {
  english = 'en',
  spanish = 'es',
}
@Component({
  selector: 'ua-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  @Input() public currentLocalization$: Observable<LanguageCode>;
  @Output() public changeLanguage: EventEmitter<string> = new EventEmitter<
    string
  >();

  desiredLocalization: LanguageCode;
  localizations: typeof LanguageCode = LanguageCode;
  presentLocalizationModal = false;
  public layoutElements$: Observable<LayoutElements>;

  constructor(
    private readonly layoutService: LayoutService,
    private readonly renderer: Renderer2,
    private readonly analyticsService: AnalyticsService,
    private readonly translateService: TranslateService,
    @Inject(DOCUMENT) private readonly document: Document,
  ) { }

  ngOnInit(): void {
    this.layoutElements$ = this.layoutService.layoutElements$;
  }

  presentLocalizationChoice(languageCode: LanguageCode, content: string): void {
    this.analyticsService.trackLocalizationInteraction(this.translateService.instant(content), languageCode);
    this.currentLocalization$.subscribe(currentLocalization => {
      if (languageCode !== currentLocalization) {
        this.presentLocalizationModal = true;
        this.desiredLocalization = languageCode;
      }
    });
  }

  confirmLocalizationChoice(): void {
    this.changeLanguage.emit(this.desiredLocalization);
    this.closeLocalizationOverlay();
  }

  closeLocalizationOverlay(): void {
    this.presentLocalizationModal = false;
    this.renderer.setStyle(this.document.body, 'overflow', 'unset');
  }
}