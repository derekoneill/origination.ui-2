import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import {
  ApplyPageKey,
  CustomerState,
  Customer,
  OnboardingState,
  ApplicationState
} from '@ua/shared/data-access';
import { BehaviorSubject, Observable, Subscription, Subject } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { AccountInfoComponent  } from './account-info.component';
import { LayoutService } from '@ua/shared/ui';
import { TranslateService } from '@ngx-translate/core';

describe('AccountInfo Component', () => {
  let component: AccountInfoComponent;

  let mockFormBuilder: SubstituteOf<FormBuilder>;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockCustomerState: SubstituteOf<CustomerState>;
  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;

  beforeEach(() => {
    mockFormBuilder = Substitute.for<FormBuilder>();
    mockLayoutService = Substitute.for<LayoutService>();
    mockCustomerState = Substitute.for<CustomerState>();
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockApplicationState = Substitute.for<ApplicationState>();
    mockOnboardingState = Substitute.for<OnboardingState>();

    component = new AccountInfoComponent(
      mockFormBuilder,
      mockLayoutService,
      mockCustomerState,
      mockApplyRouter,
      mockOnboardingState,
      mockApplicationState
    );
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});