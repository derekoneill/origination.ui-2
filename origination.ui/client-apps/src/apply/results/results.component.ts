import { formatCurrency } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AnalyticsService } from '@ua/shared/analytics';
import {
  Application,
  ApplicationState,
  Decision,
  DecisionStatus,
  OnboardingState,
  StoreLocationData
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'ua-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
})
export class ResultsComponent implements OnInit {
  // for use in template
  public decisionStatus = DecisionStatus;
  public decision$: Observable<Decision>;
  public decisionStatus$: Observable<DecisionStatus>;
  public leaseId: number;
  public leaseHeading: string;
  public application: Application;
  public storeInformation: StoreLocationData;
  private subs: Subscription[] = [];

  constructor(
    private readonly applicationState: ApplicationState,
    private readonly analyticsService: AnalyticsService,
    private readonly layout: LayoutService,
    private readonly onboardingState: OnboardingState,
    private readonly translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.layout.enableTimeout();
    this.decisionStatus$ = this.applicationState.decisionStatus$;

    this.subs.push(
      this.applicationState.application$.subscribe(
        application => (this.application = application)
      )
    );

    this.decision$ = this.applicationState.decision$.pipe(tap(decision => {}));

    this.decision$.subscribe(decision => {
      this.analyticsService.trackApplicationSubmit(decision.status, decision.leaseId, decision.approvalLimit);
      this.leaseId = decision.leaseId;
      this.leaseHeading = this.translateService.instant('pages.result.approved.leaseSummary.heading')
        .replace(
          '{{ approval_amount }}',
            `<span class="lease-summary__approval-amount">${formatCurrency(
              decision.approvalLimit,
              'en-US',
              '$',
              'USD',
              '1.0-0'
            )}</span>`
        );
    });

    this.onboardingState.onboardingInfo$.subscribe(onboardingInfo => {
      if (!onboardingInfo.storeLocation.isEcommerce) {
        this.storeInformation = onboardingInfo.storeLocation;
      }
    });

    this.layout.hideFooter();
    this.layout.hideHeaderBackButton();
    this.layout.showHeaderExitButton();
    this.layout.enableTimeout();
  }
}