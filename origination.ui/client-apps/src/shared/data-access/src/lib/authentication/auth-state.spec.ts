import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import {
  ApplicationState,
  AuthCustomer,
  AuthenticationService,
  CreditCardInformation,
  CustomerState
} from '@ua/shared/data-access';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { OnboardingState } from '../onboarding/onboarding-state';
import { AuthState } from './auth-state';

describe('Auth State', () => {
  let state: AuthState;
  let mockAuthService: SubstituteOf<AuthenticationService>;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockCustomerState: SubstituteOf<CustomerState>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;

  beforeEach(() => {
    mockAuthService = Substitute.for<AuthenticationService>();
    mockApplicationState = Substitute.for<ApplicationState>();
    mockCustomerState = Substitute.for<CustomerState>();
    mockOnboardingState = Substitute.for<OnboardingState>();

    state = new AuthState(
      mockAuthService,
      mockApplicationState,
      mockCustomerState,
      mockOnboardingState
    );
  });

  it('should create', () => {
    expect(state).toBeDefined();
  });

  describe('Handle Auth Code', () => {
    let code: string;
    let onboardingGuid: string;
    let storeIdSubject: Subject<number>;
    let jwtSubject: Subject<AuthCustomer>;
    let jwt: string;
    let language: string;

    beforeEach(() => {
      code = chance.string();
      onboardingGuid = chance.string();
      storeIdSubject = new Subject<number>();
      jwtSubject = new Subject<AuthCustomer>();
      jwt = chance.string();
      language = chance.pickone(['en', 'es']);
      mockAuthService.getJwt(Arg.any()).returns(jwtSubject);
      mockOnboardingState.storeId$.returns(storeIdSubject);
    });

    it('should send the request for the jwt', () => {
      state.handleAuthCode(code, onboardingGuid);

      mockAuthService.received().getJwt(code);
    });

    it('should set the authenticated state', () => {
      let actual: string;

      state.handleAuthCode(code, onboardingGuid);
      state.jwt.subscribe(response => (actual = response));
      jwtSubject.next({ accessToken: jwt, language } as AuthCustomer);
      storeIdSubject.next(chance.integer());

      expect(actual).toBeDefined();
    });

    it('should update what customer information we have', () => {

      const authCustomer = {
        customerId: 123
      };
      state.handleAuthCode(code, onboardingGuid);
      jwtSubject.next(authCustomer as AuthCustomer);
      storeIdSubject.next(chance.integer());

      mockCustomerState.received().loadCustomerInfo(authCustomer.customerId);
    });

    it('should update the credit card with the  customer information we have', () => {
      const authCustomer = {
        customerId: chance.integer(),
      };
      state.handleAuthCode(code, onboardingGuid);
      jwtSubject.next(authCustomer as AuthCustomer);
      storeIdSubject.next(chance.integer());

      mockCustomerState.received().updateCreditCard({ customerId: authCustomer.customerId } as CreditCardInformation);
    });

    it('should update the application with the storeId', () => {
      const authCustomer = {
        customerId: chance.integer(),
        storeId: chance.string()
      };
      state.handleAuthCode(code, onboardingGuid);
      jwtSubject.next(authCustomer as AuthCustomer);
      storeIdSubject.next(authCustomer.storeId);

      mockApplicationState.received().updateApplication({ storeId: authCustomer.storeId, customerId: authCustomer.customerId });
    });
  });
});