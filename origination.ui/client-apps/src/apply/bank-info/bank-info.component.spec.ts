import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import {
  Application,
  ApplicationState,
  ApplyPageKey,
  BankInformation,
  CustomerState
} from '@ua/shared/data-access';
import { chance } from 'jest-chance';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { BankInfoComponent, BankInfoFormFields } from './bank-info.component';
import { LayoutService } from '@ua/shared/ui';

describe('BankInfo Component', () => {
  let component: BankInfoComponent;

  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockCustomerState: SubstituteOf<CustomerState>;
  let mockFormBuilder: SubstituteOf<FormBuilder>;
  let mockLayoutService: SubstituteOf<LayoutService>;

  beforeEach(() => {
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockApplicationState = Substitute.for<ApplicationState>();
    mockCustomerState = Substitute.for<CustomerState>();
    mockFormBuilder = Substitute.for<FormBuilder>();
    mockLayoutService = Substitute.for<LayoutService>();

    component = new BankInfoComponent(
      mockApplyRouter,
      mockApplicationState,
      mockCustomerState,
      mockFormBuilder,
      mockLayoutService
    );
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('On Init', () => {
    let applicationSubject: BehaviorSubject<Application>;
    let bankSubject: BehaviorSubject<BankInformation>;
    let mockFormGroup: SubstituteOf<FormGroup>;

    beforeEach(() => {
      applicationSubject = new BehaviorSubject<Application>({} as Application);
      bankSubject = new BehaviorSubject<BankInformation>({} as BankInformation);
      mockApplicationState.application$.returns(
        applicationSubject.asObservable()
      );
      mockCustomerState.bank$.returns(bankSubject.asObservable());

      mockFormGroup = Substitute.for<FormGroup>();
      mockFormBuilder.group(Arg.any()).returns(mockFormGroup);
    });

    it('should keep track of subscriptions', () => {
      expect(component.subs.length).toBe(0);
      component.ngOnInit();

      expect(component.subs.length).toBeGreaterThan(0);
    });

    it('should add validations for the routing number field', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.bankRoutingNumber[1];
          return validators.includes(Validators.required);
        })
      );
    });

    it('should add validations for the bank account number field', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.bankAccountNumber[1];
          return validators.includes(Validators.required);
        })
      );
    });

    it('should enable timout feature', () => {
      component.ngOnInit();
      mockLayoutService.received().enableTimeout();
    });

  });

  describe('Subscription cleanup', () => {
    let mockSubscription: SubstituteOf<Subscription>;

    beforeEach(() => {
      const observable = Substitute.for<Observable<any>>();
      mockSubscription = Substitute.for<Subscription>();
      observable.subscribe().returns(mockSubscription);
      component.subs.push(observable.subscribe());
    });

    it('should unsubscribe the open subs', () => {
      mockSubscription.closed.returns(false);
      component.ngOnDestroy();

      mockSubscription.received().unsubscribe();
    });

    it('should not unsubscribe the closed subs', () => {
      mockSubscription.closed.returns(true);
      component.ngOnDestroy();

      mockSubscription.didNotReceive().unsubscribe();
    });
  });

  describe('Mask changes', () => {
    it('should toggle the masking', () => {
      const masking = chance.bool();
      component.onMaskChanged(masking);

      expect(component.isMasked).toBe(masking);
    });
  });

  describe('Continue', () => {
    let mockFormGroup: SubstituteOf<FormGroup>;

    beforeEach(() => {
      mockFormGroup = Substitute.for<FormGroup>();
      component.bankInfoForm = mockFormGroup;
    });

    describe('given a valid form', () => {
      beforeEach(() => {
        mockFormGroup.valid.returns(true);
      });

      it('should update the state', () => {
        const bankAccountNumber = chance.string();
        const bankRoutingNumber = chance.string();

        mockFormGroup
          .get(BankInfoFormFields.bankAccountNumber)
          .returns({ value: bankAccountNumber } as AbstractControl);
        mockFormGroup
          .get(BankInfoFormFields.bankRoutingNumber)
          .returns({ value: bankRoutingNumber } as AbstractControl);

        component.application = {} as Application;
        component.onContinue();

        mockCustomerState.received().updateBankInformation({
          routingNumber: bankRoutingNumber,
          accountNumber: bankAccountNumber,
        });
      });

      it('should navigate to the next page if not in edit mode', () => {
        mockLayoutService.editMode.returns(false);
        component.onContinue();
        mockApplyRouter.received().stepForward();
      });

      it('should navigate to the review page if in edit mode', () => {
        mockLayoutService.editMode.returns(true);
        component.onContinue();
        mockApplyRouter.received().navigate(ApplyPageKey.review);
      });
    });

    describe('given an invalid form', () => {
      beforeEach(() => {
        mockFormGroup.valid.returns(false);
      });

      it('should not update the state', () => {
        const bankAccountNumber = chance.string();
        const bankRoutingNumber = chance.string();

        mockFormGroup
          .get(BankInfoFormFields.bankAccountNumber)
          .returns({ value: bankAccountNumber } as AbstractControl);
        mockFormGroup
          .get(BankInfoFormFields.bankRoutingNumber)
          .returns({ value: bankRoutingNumber } as AbstractControl);

        component.application = {} as Application;
        component.onContinue();

        mockCustomerState.didNotReceive().updateBankInformation({
          routingNumber: bankRoutingNumber,
          accountNumber: bankAccountNumber,
        });
      });
    });

  });

  describe('Disallowing NonDigital Input', () => {
    let keyPressEvent;

    beforeEach(() => {
      keyPressEvent = {};
    });

    it('should reject alpha char codes', () => {
      keyPressEvent.charCode = chance.string();
      const result = component.disallowNonDigitInput(keyPressEvent);
      expect(result).toBe(false);
    });

    it('should reject alpha key codes', () => {
      keyPressEvent.charCode = undefined;
      keyPressEvent.keyCode = chance.string();
      const result = component.disallowNonDigitInput(keyPressEvent);
      expect(result).toBe(false);
    });

    it('should allow numerical char codes', () => {
      keyPressEvent.charCode = chance.pickone([
        48,
        49,
        50,
        51,
        52,
        53,
        54,
        55,
        56,
        57,
      ]);
      const result = component.disallowNonDigitInput(keyPressEvent);
      expect(result).toBeUndefined();
    });

    it('should allow numerical key codes', () => {
      keyPressEvent.charCode = undefined;
      keyPressEvent.keyCode = chance.pickone([
        48,
        49,
        50,
        51,
        52,
        53,
        54,
        55,
        56,
        57,
      ]);
      const result = component.disallowNonDigitInput(keyPressEvent);
      expect(result).toBeUndefined();
    });
  });
});