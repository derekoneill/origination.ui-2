export {};

declare global {
  export interface DataLayer {
    push: (data: { [key: string]: string | number | boolean }) => void;
  }

  interface Window {
    [key: string]: any;
    dataLayer: DataLayer;
  }
}