import { EventEmitter } from '@angular/core';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { UaLayoutComponent } from './ua-layout.component';

describe('UaLayoutComponent', () => {
  let component: UaLayoutComponent;

  beforeEach(() => {
    component = new UaLayoutComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('On Back', () => {
    let mockEmitter: SubstituteOf<EventEmitter<void>>;

    beforeEach(() => {
      mockEmitter = Substitute.for<EventEmitter<void>>();
    });

    it('should emit the back event', () => {
      component.back = mockEmitter;

      component.onBack();

      mockEmitter.received().emit();
    });
  });

  describe('On Close', () => {
    let mockEmitter: SubstituteOf<EventEmitter<void>>;

    beforeEach(() => {
      mockEmitter = Substitute.for<EventEmitter<void>>();
    });

    it('should emit the back event', () => {
      component.closeApp = mockEmitter;

      component.onClose();

      mockEmitter.received().emit();
    });
  });

  describe('On Change Language', () => {
    let mockEmitter: SubstituteOf<EventEmitter<string>>;

    beforeEach(() => {
      mockEmitter = Substitute.for<EventEmitter<string>>();
    });

    it('should emit the back event', () => {
      const testLang = 'en';
      component.changeLanguage = mockEmitter;

      component.onLanguageChange(testLang);

      mockEmitter.received().emit(testLang);
    });
  });

  describe('On Logo Click', () => {
    let mockEmitter: SubstituteOf<EventEmitter<void>>;

    beforeEach(() => {
      mockEmitter = Substitute.for<EventEmitter<void>>();
    });

    it('should emit the back event', () => {
      component.logoClick = mockEmitter;

      component.onLogoClick();

      mockEmitter.received().emit();
    });
  });
});