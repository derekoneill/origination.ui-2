import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { StoreLocationData, StoreLocatorService } from '../store-locator';
import { OnboardingService } from './onboarding.service';

@Injectable()
export class OnboardingState {
  private readonly stateSubject = new BehaviorSubject<Onboarding>({
    languageCode: 'en',
  } as Onboarding);

  public constructor(
    private readonly onboardingService: OnboardingService,
    private readonly storeLocatorService: StoreLocatorService,
  ) { }

  public get onboardingInfo$() {
    return this.stateSubject.asObservable();
  }

  public get storeId$() {
    return this.onboardingInfo$.pipe(map(state => state.storeId));
  }

  public get onboardingGuid$() {
    return this.onboardingInfo$.pipe(map(state => state.onboardingGuid));
  }

  public get languageCode$() {
    return this.onboardingInfo$.pipe(map(state => state.languageCode));
  }

  public get phoneNumber$() {
    return this.onboardingInfo$.pipe(map(state => state.phoneNumber));
  }

  public requestOnboardingInfo(onboardingGuid: string) {
    this.onboardingService
      .getOnboardingInfo(onboardingGuid)
      .pipe(mergeMap(onboarding => this.storeLocatorService.getStoreLocationData(onboarding.storeId),
      (onboardingInfo, storeInfo) => {
        const onboardingData: Onboarding = {
          ...onboardingInfo,
          storeLocation: storeInfo
        };
        return onboardingData;
      }
      ))
      .subscribe(onboarding => {
        this.stateSubject.next(onboarding);
      });
  }

  public setLanguage(languageCode: string) {
    if (this.getSnapshot().onboardingGuid) {
      this.onboardingService
        .updateLanguage(this.getSnapshot().onboardingGuid, languageCode);
    }

    this.stateSubject.next({
      ...this.getSnapshot(),
      languageCode,
    });
  }

  private getSnapshot(): Onboarding {
    return this.stateSubject.value;
  }
}

export interface Onboarding {
  onboardingGuid: string;
  languageCode: string;
  originatingPlatform: string;
  phoneNumber: string;
  storeId: number;
  storeLocation: StoreLocationData;
}