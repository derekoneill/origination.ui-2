import { NgModule } from '@angular/core';
import { AnalyticsService } from './analytics.service';
import { EmitButtonClickDirective } from './emit-button-click-directive';
import { EmitFormElementEngagementDirective } from './emit-form-element-engagement.directive';
import { EmitLinkClickDirective } from './emit-link-click.directive';

@NgModule({
  imports: [],
  declarations: [
    EmitButtonClickDirective,
    EmitLinkClickDirective,
    EmitFormElementEngagementDirective,
  ],
  providers: [AnalyticsService],
  exports: [
    EmitButtonClickDirective,
    EmitLinkClickDirective,
    EmitFormElementEngagementDirective,
  ],
})
export class AnalyticsModule {}