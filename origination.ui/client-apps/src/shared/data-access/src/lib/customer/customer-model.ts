import { ApplicationPayFrequency } from '../application';

export interface Customer {
  customerId: number;
  firstName: string;
  lastName: string;
  address: Address;
  bank: BankInformation;
  incomeSource: IncomeInformation;
  birthdate: string;
  emailAddress: string;
  cellPhone: string;
  ssn: string;
  last4Ssn: string;
  marketingOptIn: boolean;
}

export interface Address {
  address1: string;
  address2: string;
  state: string;
  city: string;
  zip: string;
}

export interface BankInformation {
  routingNumber: string;
  accountNumber: string;
}

export interface IncomeInformation {
  payFrequency: ApplicationPayFrequency;
  monthlyGrossIncome: number;
  lastPaymentDate: string;
  nextPaymentDate: string;
}

export interface CreditCardInformation {
  customerId: number;
  cardNumber: string;
  token: string;
  bin: string;
  expirationDate: string;
  firstName: string;
  lastName: string;
  streetAddress1: string;
  streetAddress2: string;
  city: string;
  state: string;
  zip: string;
}

export interface SaveCreditCardResponse {
  customerId: number;
  token: string;
  bin: string;
  firstName: string;
  lastName: string;
  expirationDate: string;
}

export interface CreditCardTokenResponse {
  successful: boolean;
  message: string;
  statusCode: number;
  token: string;
  bin: number;
}

export enum CustomerStatus {
  new = 'new'
}