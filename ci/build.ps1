$currentLeaf = Split-Path -Path (Get-Location) -Leaf
if ($currentLeaf -ne "origination.ui") {
  Throw "Please run the scripted commands from the root dotnet location. Ran them from $currentLocation"
}

$now = Get-Date -Format "M.d.y_HH.mm.s.f"
$distFolderName = "dist/deploy_$now";

dotnet publish -c Release -r win-x64 --self-contained true /p:AppendBaseHref=true /p:useapphost=true --no-dependencies -v:q -o $distFolderName