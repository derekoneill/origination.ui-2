Function CreateEc2Instance {
    Import-Module "$PSScriptRoot/Setup-Aws.psm1" -force

    SetupAws > $null

    $reservation = New-EC2Instance `
        -ImageId ami-0d9f0160a6ff8a4e7 `
        -MinCount 1 -MaxCount 1 `
        -Region us-east-1 `
        -KeyName ProgApply `
        -SecurityGroupId sg-03d1c3c5822f74442 `
        -InstanceType t2.micro `
        -SubnetId subnet-a91f9d82

    $instanceId = $reservation.Instances.InstanceId
    
    Write-Host "Waiting for instance $instanceId to start..."
    while ((Get-EC2InstanceStatus -IncludeAllInstance $true -InstanceId $instanceId).InstanceState.Name.Value -ne "running") {
        if ($attempts++ -eq 20) {
            v
            throw "That's enough, I'm giving up. The instance wouldn't start"
        }
        Write-Verbose "Waiting for our instance to be running..."
        Start-Sleep -Seconds 3
    }
    
    $instance = GetInstanceByInstanceId $instanceId
    $instanceIp = $instance.PublicIpAddress
    $sshAvailable = $false;
    $attempts = 0;
    Write-Host "Waiting for OS to start..."
    while ($sshAvailable -eq $false) {
        if ($attempts++ -eq 10) {
            throw "That's enough, I'm giving up. I couldn't connect to the instance"
        }
        ssh -i "$PSScriptRoot/ProgApply.pem"  -o StrictHostKeyChecking=no  "ec2-user@${instanceIp}" exit 
        if ($LASTEXITCODE -eq 0) {
            $sshAvailable = $true;
        }
        Write-Verbose "Waiting for our instance to be available..."
        Start-Sleep -Seconds 3
    }
    return "$instanceId"
}

Function TerminateEc2Instance {
    param(
        [parameter(mandatory = $true, position = 0)]
        [string]$InstanceId
    )

    Import-Module "$PSScriptRoot/Setup-Aws.psm1" -force
    SetupAws

    Import-Module "$PSScriptRoot/Production-Host.psm1" -force

    $prodId = GetProdInstanceId
    if ($prodId -eq $InstanceId) {
        throw "Do not terminate the production host"
    }

    "Terminating AWS instance $InstanceId"
    Remove-EC2Instance -InstanceId $InstanceId -Force
}

Function GetInstanceByIp {      
    param(
        [parameter(mandatory = $true, position = 0)]
        [ValidateNotNullOrEmpty()]
        [string]$ipAddress
    )
    
    $allInstances = GetAllInstances
    $instance = $allInstances | Where-Object( { $_.PublicIpAddress -eq $ipAddress })
    $instance
}

Function GetInstanceByInstanceId {      
    param(
        [parameter(mandatory = $true, position = 0)]
        [ValidateNotNullOrEmpty()]
        [string]$instanceId
    )
    
    $allInstances = GetAllInstances
    $instance = $allInstances | Where-Object( { $_.InstanceId -eq $instanceId })
    $instance
}

Function GetAllInstances {    
    Import-Module "$PSScriptRoot/Setup-Aws.psm1" -force
    SetupAws
    $reservation = Get-EC2Instance
    $all = $reservation.Instances
    $all
}

Export-ModuleMember -Function CreateEc2Instance
Export-ModuleMember -Function TerminateEc2Instance
Export-ModuleMember -Function GetInstanceByIp
Export-ModuleMember -Function GetInstanceByInstanceId
Export-ModuleMember -Function GetAllInstances