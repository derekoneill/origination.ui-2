export enum ClickEventType {
    button,
    link,
    paymentEstimatorButton
}

export enum FormElementEngagementEventType {
    formElement,
    formFill
}