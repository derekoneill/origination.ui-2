Function FileContentsMatch([string] $filePath1, [string] $filePath2) {
  $contents1 = Get-FileHash $filePath1
  $contents2 = Get-FileHash $filePath2

  $match = $contents1.hash -eq $contents2.hash
  if ($match) {
    return $true
  }
  return $false
}