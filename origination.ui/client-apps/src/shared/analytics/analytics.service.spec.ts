import { fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { NavigationEnd, PRIMARY_OUTLET, Router, RouterEvent, UrlSegment, UrlSegmentGroup, UrlTree } from '@angular/router';
import Substitute, { SubstituteOf } from '@fluffy-spoon/substitute';
import { ApplicationState, Customer, CustomerState, Decision } from '@ua/shared/data-access';
import { chance } from 'jest-chance';
import MockDate from 'mockdate';
import { Subject } from 'rxjs';
import { Onboarding, OnboardingState } from '../data-access/src/lib/onboarding/onboarding-state';
import { ClickEventType, FormElementEngagementEventType } from './analytics-event-emission-enums';
import { ClickEventEmission, ErrorEventEmission, FormElementEmission } from './analytics-event-emission-interfaces';
import { AnalyticsService } from './analytics.service';

describe('AnalyticsService', () => {
  let service: AnalyticsService;
  let mockOnboardingState: SubstituteOf<OnboardingState>;
  let mockCustomerState: SubstituteOf<CustomerState>;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockRouter: SubstituteOf<Router>;

  let mockEventSubject: Subject<RouterEvent>;
  let mockOnboardingSubject: Subject<Onboarding>;
  let mockCustomerSubject: Subject<Customer>;
  let mockApplicationResultSubject: Subject<Decision>;
  const url = '/sample/route?sample=sample';

  beforeEach(() => {
    mockOnboardingState = Substitute.for<OnboardingState>();
    mockCustomerState = Substitute.for<CustomerState>();
    mockApplicationState = Substitute.for<ApplicationState>();
    mockRouter = Substitute.for<Router>();
    mockEventSubject = new Subject<RouterEvent>();
    mockOnboardingSubject = new Subject<Onboarding>();
    mockCustomerSubject = new Subject<Customer>();
    mockApplicationResultSubject = new Subject<Decision>();

    mockRouter.events.returns(mockEventSubject.asObservable());
    mockOnboardingState.onboardingInfo$.returns(mockOnboardingSubject.asObservable());
    mockCustomerState.customer$.returns(mockCustomerSubject.asObservable());
    mockApplicationState.decision$.returns(mockApplicationResultSubject.asObservable());
    mockRouter.url.returns(url);
    mockRouter.parseUrl(url).returns({
      root: {
        children: {
          [PRIMARY_OUTLET]: {
            segments: [
              { path: 'sample' } as UrlSegment,
              { path: 'route' } as UrlSegment
            ] as UrlSegment[]
          } as UrlSegmentGroup
        } as any
      } as UrlSegmentGroup
    } as UrlTree);

    service = new AnalyticsService(mockOnboardingState, mockCustomerState, mockApplicationState, mockRouter);
    service.analyticsWindow.dataLayer = [];
  });

  afterEach(() => {
    service.analyticsWindow.dataLayer = [];
  });

  it('should be created', () => {
    expect(service).toBeDefined();
  });

  it('should place a button click event into the dataLayer', () => {
    const eventLabel = chance.string();
    const eventValue = chance.integer({ min: 0, max: 1000 });
    const clickEvent: ClickEventEmission = {
      eventProperties: {
        eventLabel,
        eventValue
      },
      type: ClickEventType.button
    };
    service.trackUniversalButtonClick(clickEvent);
    expect(service.analyticsWindow.dataLayer[0].eventLabel).toBe(eventLabel);
    expect(service.analyticsWindow.dataLayer[0].eventValue).toBe(eventValue);
  });

  it('should place a link click event into the dataLayer', () => {
    const eventLabel = chance.string();
    const eventValue = chance.integer({ min: 0, max: 1000 });
    const clickEvent: ClickEventEmission = {
      eventProperties: {
        eventLabel,
        eventValue
      },
      type: ClickEventType.button
    };
    service.trackUniversalLinkClick(clickEvent);
    expect(service.analyticsWindow.dataLayer[0].eventLabel).toBe(eventLabel);
    expect(service.analyticsWindow.dataLayer[0].eventValue).toBe(eventValue);
  });

  it('should place a form element engagement event into the dataLayer', () => {
    const eventLabel = chance.string();
    const eventValue = chance.integer({ min: 0, max: 1000 });
    const formEngagementEvent: FormElementEmission = {
      eventProperties: {
        eventLabel,
        eventValue
      },
      type: FormElementEngagementEventType.formElement
    };
    service.trackFormElementEngagement(formEngagementEvent);
    expect(service.analyticsWindow.dataLayer[0].eventLabel).toBe(eventLabel);
    expect(service.analyticsWindow.dataLayer[0].eventValue).toBe(eventValue);
  });

  it('should place a form fill event into the dataLayer', () => {
    const eventLabel = chance.string();
    const eventValue = chance.integer({ min: 0, max: 1000 });
    const formEngagementEvent: FormElementEmission = {
      eventProperties: {
        eventLabel,
        eventValue
      },
      type: FormElementEngagementEventType.formFill
    };
    service.trackFormFill(formEngagementEvent);
    expect(service.analyticsWindow.dataLayer[0].eventLabel).toBe(eventLabel);
    expect(service.analyticsWindow.dataLayer[0].eventValue).toBe(eventValue);
    expect(service.analyticsWindow.dataLayer[0].formFieldName).toBe(eventLabel);
  });

  it('should place an error event into the dataLayer', () => {
    const errorMessage = chance.string();
    const errorType = chance.string();
    const errorEvent: ErrorEventEmission = {
      errorMessage,
      errorType
    };
    service.trackError(errorEvent);
    expect(service.analyticsWindow.dataLayer[0].eventAction).toBe(errorType);
    expect(service.analyticsWindow.dataLayer[0].eventLabel).toBe(errorMessage);
  });

  it('should place an application start event into the dataLayer', () => {
    const isResuming = chance.bool();
    const isReApplying = chance.bool();
    const expectedResume = isResuming ? 1 : 0;
    const expectedReApply = isReApplying ? 1 : 0;

    service.trackApplicationStart(isResuming, isReApplying);

    expect(service.analyticsWindow.dataLayer[0].applicationReapply).toBe(expectedReApply);
    expect(service.analyticsWindow.dataLayer[0].applicationsResumed).toBe(expectedResume);
  });

  it('should place an application submit event into the dataLayer', () => {
    const approvalStatus = chance.string();
    const leaseId = chance.integer({ min: 0 });
    const approvalAmount = chance.integer({ min: 0 });
    const applicationDurationInSeconds = chance.integer({ min: 1, max: 500 });
    const applicationEndTime = new Date(new Date().getTime() + applicationDurationInSeconds * 1000);

    service.trackApplicationStart();
    MockDate.set(applicationEndTime);
    service.trackApplicationSubmit(approvalStatus, leaseId, approvalAmount);

    expect(service.analyticsWindow.dataLayer[1].approvalStatus).toBe(approvalStatus);
    expect(service.analyticsWindow.dataLayer[1].leaseId).toBe(leaseId);
    expect(service.analyticsWindow.dataLayer[1].approvalAmount).toBe(approvalAmount);
    expect(service.analyticsWindow.dataLayer[1].applicationCompletionTime).toBe(applicationDurationInSeconds);
  });

  it('should place a localization interaction event into the dataLayer', () => {
    const eventLabel = chance.string();
    const languageCode = chance.pickone(['en', 'es']);

    service.trackLocalizationInteraction(eventLabel, languageCode);

    expect(service.analyticsWindow.dataLayer[0].eventLabel).toBe(eventLabel);
    expect(service.analyticsWindow.dataLayer[0].localizationLanguage).toBe(languageCode);
  });

  describe('Given a page event with no onboarding guid', () => {

    beforeEach(waitForAsync(() => {
      mockCustomerSubject.next({} as Customer);
      mockApplicationResultSubject.next({} as Decision);
      mockOnboardingSubject.next({} as Onboarding);
      mockEventSubject.next(new NavigationEnd(undefined, undefined, undefined));
    }));

    it('should place no pageview event into the dataLayer', fakeAsync(() => {
      tick(1);
      expect(service.analyticsWindow.dataLayer[0]).toBeUndefined();
    }));
  });

  describe('Given a page event with only an onboarding guid', () => {
    let mockOnboardingGuid: string;
    let mockStoreId: number;

    beforeEach(waitForAsync(() => {
      mockOnboardingGuid = chance.guid();
      mockStoreId = chance.integer({ min: 0 });
      mockCustomerSubject.next({} as Customer);
      mockApplicationResultSubject.next({} as Decision);
      mockOnboardingSubject.next({
        onboardingGuid: mockOnboardingGuid,
        storeId: mockStoreId
      } as Onboarding);
      mockEventSubject.next(new NavigationEnd(undefined, undefined, undefined));
    }));

    it('should place a default pageview event into the dataLayer', fakeAsync(() => {
      tick(1);
      expect(service.analyticsWindow.dataLayer[0].page).toBe('/sample/route');
      expect(service.analyticsWindow.dataLayer[0].title).toBe('route');
      expect(service.analyticsWindow.dataLayer[0].onboardingGuid).toBe(mockOnboardingGuid);
      expect(service.analyticsWindow.dataLayer[0].storeId).toBe(mockStoreId);
      expect(service.analyticsWindow.dataLayer[0].customerId).toBe(undefined);
      expect(service.analyticsWindow.dataLayer[0].applicationId).toBe(undefined);
    }));
  });

  describe('Given a page event with an onboarding guid, customer id and application id', () => {
    let mockOnboardingGuid: string;
    let mockStoreId: number;
    let mockCustomerId: number;
    let mockApplicationId: number;

    beforeEach(waitForAsync(() => {
      mockOnboardingGuid = chance.guid();
      mockStoreId = chance.integer({ min: 0 });
      mockCustomerId = chance.integer({ min: 0 });
      mockApplicationId = chance.integer({ min: 0 });
      mockCustomerSubject.next({ customerId: mockCustomerId } as Customer);
      mockApplicationResultSubject.next({ leaseId: mockApplicationId } as Decision);
      mockOnboardingSubject.next({
        onboardingGuid: mockOnboardingGuid,
        storeId: mockStoreId
      } as Onboarding);
      mockEventSubject.next(new NavigationEnd(undefined, undefined, undefined));
    }));

    it('should place a full pageview event into the dataLayer', fakeAsync(() => {
      tick(1);
      expect(service.analyticsWindow.dataLayer[0].customerId).toBe(mockCustomerId);
      expect(service.analyticsWindow.dataLayer[0].applicationId).toBe(mockApplicationId);
    }));
  });
});